var data = [
  {
    "style": 0,
    "id": 1,
    "name": "C\u00e0 ph\u00ea",
    "title": "C\u00e0 ph\u00ea",
    "thumbnail": "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/ca-phe.png",
    "slug": "ca-phe",
    "products": [
      {
        "id": "5b03966a1acd4d5bbd6723a3",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea S\u1eefa \u0110\u00e1",
        "description": "C\u00e0 ph\u00ea \u0110\u1eafk L\u1eafk nguy\u00ean ch\u1ea5t \u0111\u01b0\u1ee3c pha phin truy\u1ec1n th\u1ed1ng k\u1ebft h\u1ee3p v\u1edbi s\u1eefa \u0111\u1eb7c t\u1ea1o n\u00ean h\u01b0\u01a1ng v\u1ecb \u0111\u1eadm \u0111\u00e0, h\u00e0i h\u00f2a gi\u1eefa v\u1ecb ng\u1ecdt \u0111\u1ea7u l\u01b0\u1ee1i v\u00e0 v\u1ecb \u0111\u1eafng thanh tho\u00e1t n\u01a1i h\u1eadu v\u1ecb.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": "180",
                "code": "10020012",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 180
              },
              {
                "product_id": "179",
                "code": "10020011",
                "price": 6000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 6.000\u0111",
                "name": "V\u1eeba",
                "id": 179
              },
              {
                "code": "10020222",
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "price": 10000,
                "product_id": 10020222,
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 10.000\u0111",
                "name": "L\u1edbn",
                "id": 10020222
              }
            ]
          }
        ],
        "price": 17400,
        "base_price_str": "29.000\u0111",
        "price_str": "17.400\u0111",
        "base_price": 29000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639377738_ca-phe-sua-da_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639377738_ca-phe-sua-da.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00cdt s\u1eefa",
          "\u00edt \u0111\u00e1",
          "\u00edt ng\u1ecdt",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-sua-da"
      },
      {
        "id": "5b03966a1acd4d5bbd6723a2",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea S\u1eefa N\u00f3ng",
        "description": "C\u00e0 ph\u00ea \u0111\u01b0\u1ee3c pha phin truy\u1ec1n th\u1ed1ng k\u1ebft h\u1ee3p v\u1edbi s\u1eefa \u0111\u1eb7c t\u1ea1o n\u00ean h\u01b0\u01a1ng v\u1ecb \u0111\u1eadm \u0111\u00e0, h\u00e0i h\u00f2a gi\u1eefa v\u1ecb ng\u1ecdt \u0111\u1ea7u l\u01b0\u1ee1i v\u00e0 v\u1ecb \u0111\u1eafng thanh tho\u00e1t n\u01a1i h\u1eadu v\u1ecb.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 10020223,
                "code": "10020223",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba ",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10020223
              }
            ]
          }
        ],
        "price": 21000,
        "base_price_str": "35.000\u0111",
        "price_str": "21.000\u0111",
        "base_price": 35000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639377770_cfsua-nong_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639377770_cfsua-nong.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00cdt s\u1eefa",
          "\u00edt \u0111\u00e1",
          "\u0110\u00e1 \u0111\u1ec3 ri\u00eang",
          "\u00edt ng\u1ecdt",
          "Kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-sua-nong"
      },
      {
        "id": "5b03966a1acd4d5bbd6723a4",
        "recommended_id": null,
        "name": "B\u1ea1c S\u1ec9u",
        "description": "B\u1ea1c s\u1ec9u ch\u00ednh l\u00e0 \"Ly s\u1eefa tr\u1eafng k\u00e8m m\u1ed9t ch\u00fat c\u00e0 ph\u00ea\". Th\u1ee9c u\u1ed1ng n\u00e0y r\u1ea5t ph\u00f9 h\u1ee3p nh\u1eefng ai v\u1eeba mu\u1ed1n tr\u1ea3i nghi\u1ec7m ch\u00fat v\u1ecb \u0111\u1eafng c\u1ee7a c\u00e0 ph\u00ea v\u1eeba mu\u1ed1n th\u01b0\u1edfng th\u1ee9c v\u1ecb ng\u1ecdt b\u00e9o ng\u1eady t\u1eeb s\u1eefa.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020006,
                "code": "10020006",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020006
              },
              {
                "product_id": 10020005,
                "code": "10020005",
                "price": 6000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 6.000\u0111",
                "name": "V\u1eeba",
                "id": 10020005
              },
              {
                "code": "10020226",
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "price": 10000,
                "product_id": 10020226,
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 10.000\u0111",
                "name": "L\u1edbn",
                "id": 10020226
              }
            ]
          }
        ],
        "price": 17400,
        "base_price_str": "29.000\u0111",
        "price_str": "17.400\u0111",
        "base_price": 29000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639377904_bac-siu_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639377904_bac-siu.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "\u00edt \u0111\u00e1",
          "nhi\u1ec1u s\u1eefa",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "bac-siu"
      },
      {
        "id": "5cdbd6e8696fb3792a389754",
        "recommended_id": null,
        "name": "B\u1ea1c S\u1ec9u N\u00f3ng",
        "description": "B\u1ea1c s\u1ec9u ch\u00ednh l\u00e0 \"Ly s\u1eefa tr\u1eafng k\u00e8m m\u1ed9t ch\u00fat c\u00e0 ph\u00ea\". Th\u1ee9c u\u1ed1ng n\u00e0y r\u1ea5t ph\u00f9 h\u1ee3p nh\u1eefng ai v\u1eeba mu\u1ed1n tr\u1ea3i nghi\u1ec7m ch\u00fat v\u1ecb \u0111\u1eafng c\u1ee7a c\u00e0 ph\u00ea v\u1eeba mu\u1ed1n th\u01b0\u1edfng th\u1ee9c v\u1ecb ng\u1ecdt b\u00e9o ng\u1eady t\u1eeb s\u1eefa.\n",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10020227",
                "localize": {
                  "en": "Nh\u1ecf",
                  "vi": "Nh\u1ecf"
                },
                "price": 0,
                "product_id": 10020227,
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10020227
              }
            ]
          }
        ],
        "price": 21000,
        "base_price_str": "35.000\u0111",
        "price_str": "21.000\u0111",
        "base_price": 35000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639377926_bacsiunong_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639377926_bacsiunong.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "\u00edt \u0111\u00e1",
          "Nhi\u1ec1u s\u1eefa",
          "Kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "bac-siu-nong"
      },
      {
        "id": "5b03966a1acd4d5bbd6723a1",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea \u0110en \u0110\u00e1",
        "description": "Kh\u00f4ng ng\u1ecdt ng\u00e0o nh\u01b0 B\u1ea1c s\u1ec9u hay C\u00e0 ph\u00ea s\u1eefa, C\u00e0 ph\u00ea \u0111en mang trong m\u00ecnh phong v\u1ecb tr\u1ea7m l\u1eafng, thi v\u1ecb h\u01a1n. Ng\u01b0\u1eddi ta th\u01b0\u1eddng ph\u1ea3i ng\u1ed3i r\u1ea5t l\u00e2u m\u1edbi c\u1ea3m nh\u1eadn \u0111\u01b0\u1ee3c h\u1ebft h\u01b0\u01a1ng th\u01a1m ng\u00e0o ng\u1ea1t, ph\u1ea3ng ph\u1ea5t m\u00f9i cacao v\u00e0 c\u00e1i \u0111\u1eafng m\u01b0\u1ee3t m\u00e0 tr\u00f4i tu\u1ed9t xu\u1ed1ng v\u00f2m h\u1ecdng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020008,
                "code": "10020008",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020008
              },
              {
                "product_id": 10020007,
                "code": "10020007",
                "price": 6000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 6.000\u0111",
                "name": "V\u1eeba",
                "id": 10020007
              },
              {
                "code": "10020224",
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "price": 10000,
                "product_id": 10020224,
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 10.000\u0111",
                "name": "L\u1edbn",
                "id": 10020224
              }
            ]
          }
        ],
        "price": 17400,
        "base_price_str": "29.000\u0111",
        "price_str": "17.400\u0111",
        "base_price": 29000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639377798_ca-phe-den-da_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639377797_ca-phe-den-da.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt \u0111\u00e1",
          "\u00edt ng\u1ecdt",
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-den-da"
      },
      {
        "id": "5b03966a1acd4d5bbd6723a0",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea \u0110en N\u00f3ng",
        "description": "Kh\u00f4ng ng\u1ecdt ng\u00e0o nh\u01b0 B\u1ea1c s\u1ec9u hay C\u00e0 ph\u00ea s\u1eefa, C\u00e0 ph\u00ea \u0111en mang trong m\u00ecnh phong v\u1ecb tr\u1ea7m l\u1eafng, thi v\u1ecb h\u01a1n. Ng\u01b0\u1eddi ta th\u01b0\u1eddng ph\u1ea3i ng\u1ed3i r\u1ea5t l\u00e2u m\u1edbi c\u1ea3m nh\u1eadn \u0111\u01b0\u1ee3c h\u1ebft h\u01b0\u01a1ng th\u01a1m ng\u00e0o ng\u1ea1t, ph\u1ea3ng ph\u1ea5t m\u00f9i cacao v\u00e0 c\u00e1i \u0111\u1eafng m\u01b0\u1ee3t m\u00e0 tr\u00f4i tu\u1ed9t xu\u1ed1ng v\u00f2m h\u1ecdng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 10020225,
                "code": "10020225",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10020225
              }
            ]
          }
        ],
        "price": 21000,
        "base_price_str": "35.000\u0111",
        "price_str": "21.000\u0111",
        "base_price": 35000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639377816_ca-phe-den-nong_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639377816_ca-phe-den-nong.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-den-nong"
      },
      {
        "id": "61a4f0f461af54001a4619ff",
        "recommended_id": null,
        "name": "Latte T\u00e1o L\u00ea Qu\u1ebf (\u0110\u00e1)",
        "description": "S\u1ef1 k\u1ebft h\u1ee3p gi\u1eefa c\u00e0 ph\u00ea \u0111\u1eadm \u0111\u00e0 v\u1edbi v\u1ecb ng\u1ecdt cay nh\u1eb9 c\u1ee7a qu\u1ebf h\u00f2a quy\u1ec7n c\u00f9ng v\u1ecb chua d\u1ecbu c\u1ee7a t\u00e1o l\u00ea t\u1ea1o n\u00ean m\u1ed9t th\u1ee9c u\u1ed1ng tuy\u1ec7t h\u1ea3o, \u0111\u1ea7y th\u00fa v\u1ecb.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10040034",
                "price": 0,
                "product_id": 2638,
                "val": "V\u1eeba",
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2638
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 1456,
                "product_name": "Extra foam",
                "code": "10100024",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Extra foam",
                "id": 1456
              },
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              },
              {
                "product_id": 1667,
                "product_name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "code": "10100016",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "id": 1667
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1642389499_1638267966-latte-tao-le-que-lanh_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1642389499_1638267966-latte-tao-le-que-lanh.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "latte-tao-le-que-da"
      },
      {
        "id": "61a4f0f461af54001a4619fe",
        "recommended_id": null,
        "name": "Latte T\u00e1o L\u00ea Qu\u1ebf (N\u00f3ng)",
        "description": "S\u1ef1 k\u1ebft h\u1ee3p gi\u1eefa c\u00e0 ph\u00ea \u0111\u1eadm \u0111\u00e0 v\u1edbi v\u1ecb ng\u1ecdt cay nh\u1eb9 c\u1ee7a qu\u1ebf h\u00f2a quy\u1ec7n c\u00f9ng v\u1ecb chua d\u1ecbu c\u1ee7a t\u00e1o l\u00ea t\u1ea1o n\u00ean m\u1ed9t th\u1ee9c u\u1ed1ng tuy\u1ec7t h\u1ea3o, \u0111\u1ea7y th\u00fa v\u1ecb.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10040036",
                "price": 0,
                "product_id": 2640,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2640
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1642389486_1638333813-latte-tao-le-que-nong_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1642389486_1638333813-latte-tao-le-que-nong.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "latte-tao-le-que-nong"
      },
      {
        "id": "61a4f0f461af54001a4619fd",
        "recommended_id": null,
        "name": "Latte T\u00e1o L\u00ea Qu\u1ebf Chai Fresh 500ml",
        "description": "Phi\u00ean b\u1ea3n Chai Fresh ti\u1ec7n l\u1ee3i, v\u1edbi th\u1ee9c u\u1ed1ng \u0111\u1eadm \u0111\u00e0, th\u00fa v\u1ecb tuy\u1ec7t h\u1ea3o \u0111\u1ec3 c\u00f9ng b\u1ea1n t\u1eadn h\u01b0\u1edfng nh\u1eefng ng\u00e0y cu\u1ed1i n\u0103m \u1ea5m \u00e1p v\u00e0 tr\u1ecdn v\u1eb9n.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10040037",
                "price": 0,
                "product_id": 2641,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2641
              }
            ]
          }
        ],
        "price": 65400,
        "base_price_str": "109.000\u0111",
        "price_str": "65.400\u0111",
        "base_price": 109000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1642389471_1638596199-bottle-lattetaoleque-new_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1642389471_1638596199-bottle-lattetaoleque-new.jpg"
        ],
        "is_pickup": false,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "latte-tao-le-que-chai-fresh-500ml"
      },
      {
        "id": "61534bde26ae260012abe219",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea S\u1eefa \u0110\u00e1 Chai Fresh 250ML",
        "description": "V\u1eabn l\u00e0 h\u01b0\u01a1ng v\u1ecb c\u00e0 ph\u00ea s\u1eefa \u0111\u1eadm \u0111\u00e0 quen thu\u1ed9c c\u1ee7a The Coffee House nh\u01b0ng kho\u00e1c l\u00ean m\u00ecnh m\u1ed9t chi\u1ebfc \u00e1o m\u1edbi ti\u1ec7n l\u1ee3i h\u01a1n, ti\u1ebft ki\u1ec7m h\u01a1n ph\u00f9 h\u1ee3p v\u1edbi b\u00ecnh th\u01b0\u1eddng m\u1edbi, gi\u00fap b\u1ea1n t\u1eadn h\u01b0\u1edfng m\u1ed9t ng\u00e0y d\u00e0i tr\u1ecdn v\u1eb9n.\n*S\u1ea3n ph\u1ea9m d\u00f9ng ngon nh\u1ea5t trong ng\u00e0y.\n*S\u1ea3n ph\u1ea9m m\u1eb7c \u0111\u1ecbnh m\u1ee9c \u0111\u01b0\u1eddng v\u00e0 kh\u00f4ng \u0111\u00e1.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10020399",
                "price": 0,
                "product_id": 2604,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2604
              }
            ]
          }
        ],
        "price": 47400,
        "base_price_str": "79.000\u0111",
        "price_str": "47.400\u0111",
        "base_price": 79000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/BottleCFSD_496652_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/BottleCFSD_496652.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1633146193_photo-2021-10-02-10-42-41.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1633146193_photo-2021-10-02-10-42-46.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1633146194_photo-2021-10-02-10-42-28.jpg"
        ],
        "is_pickup": false,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-sua-da-chai-fresh-250ml"
      },
      {
        "id": "5b03966a1acd4d5bbd67237b",
        "recommended_id": null,
        "name": "Caramel Macchiato \u0110\u00e1",
        "description": "Caramel Macchiato s\u1ebd mang \u0111\u1ebfn m\u1ed9t s\u1ef1 ng\u1ea1c nhi\u00ean th\u00fa v\u1ecb khi v\u1ecb th\u01a1m b\u00e9o c\u1ee7a b\u1ecdt s\u1eefa, s\u1eefa t\u01b0\u01a1i, v\u1ecb \u0111\u1eafng thanh tho\u00e1t c\u1ee7a c\u00e0 ph\u00ea Espresso h\u1ea3o h\u1ea1ng v\u00e0 v\u1ecb ng\u1ecdt \u0111\u1eadm c\u1ee7a s\u1ed1t caramel \u0111\u01b0\u1ee3c g\u00f3i g\u1ecdn trong m\u1ed9t t\u00e1ch c\u00e0 ph\u00ea.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020020,
                "code": "10020020",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020020
              },
              {
                "product_id": 10020019,
                "code": "10020019",
                "price": 5000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "V\u1eeba",
                "id": 10020019
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              },
              {
                "product_id": 10100019,
                "product_name": "Sauce Caramel",
                "code": "10100019",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Sauce Caramel",
                "id": 10100019
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/caramel-macchiato_143623_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/caramel-macchiato_143623.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "\u00edt \u0111\u00e1",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "caramel-macchiato-da"
      },
      {
        "id": "5b03966a1acd4d5bbd67237a",
        "recommended_id": null,
        "name": "Caramel Macchiato N\u00f3ng",
        "description": "Caramel Macchiato s\u1ebd mang \u0111\u1ebfn m\u1ed9t s\u1ef1 ng\u1ea1c nhi\u00ean th\u00fa v\u1ecb khi v\u1ecb th\u01a1m b\u00e9o c\u1ee7a b\u1ecdt s\u1eefa, s\u1eefa t\u01b0\u01a1i, v\u1ecb \u0111\u1eafng thanh tho\u00e1t c\u1ee7a c\u00e0 ph\u00ea Espresso h\u1ea3o h\u1ea1ng v\u00e0 v\u1ecb ng\u1ecdt \u0111\u1eadm c\u1ee7a s\u1ed1t caramel \u0111\u01b0\u1ee3c g\u00f3i g\u1ecdn trong m\u1ed9t t\u00e1ch c\u00e0 ph\u00ea.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020022,
                "code": "10020022",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020022
              },
              {
                "product_id": 10020021,
                "code": "10020021",
                "price": 5000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "V\u1eeba",
                "id": 10020021
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              },
              {
                "product_id": 10100019,
                "product_name": "Sauce Caramel",
                "code": "10100019",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Sauce Caramel",
                "id": 10100019
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/caramelmacchiatonong_168039_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/caramelmacchiatonong_168039.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "caramel-macchiato-nong"
      },
      {
        "id": "5b03966a1acd4d5bbd672391",
        "recommended_id": null,
        "name": "Latte \u0110\u00e1",
        "description": "M\u1ed9t s\u1ef1 k\u1ebft h\u1ee3p tinh t\u1ebf gi\u1eefa v\u1ecb \u0111\u1eafng c\u00e0 ph\u00ea Espresso nguy\u00ean ch\u1ea5t h\u00f2a quy\u1ec7n c\u00f9ng v\u1ecb s\u1eefa n\u00f3ng ng\u1ecdt ng\u00e0o, b\u00ean tr\u00ean l\u00e0 m\u1ed9t l\u1edbp kem m\u1ecfng nh\u1eb9 t\u1ea1o n\u00ean m\u1ed9t t\u00e1ch c\u00e0 ph\u00ea ho\u00e0n h\u1ea3o v\u1ec1 h\u01b0\u01a1ng v\u1ecb l\u1eabn nh\u00e3n quan.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020032,
                "code": "10020032",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020032
              },
              {
                "product_id": 10020033,
                "code": "10020033",
                "price": 5000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "V\u1eeba",
                "id": 10020033
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/latte-da_438410_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/latte-da_438410.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "\u00edt \u0111\u00e1",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "latte-da"
      },
      {
        "id": "5b03966a1acd4d5bbd672390",
        "recommended_id": null,
        "name": "Latte N\u00f3ng",
        "description": "M\u1ed9t s\u1ef1 k\u1ebft h\u1ee3p tinh t\u1ebf gi\u1eefa v\u1ecb \u0111\u1eafng c\u00e0 ph\u00ea Espresso nguy\u00ean ch\u1ea5t h\u00f2a quy\u1ec7n c\u00f9ng v\u1ecb s\u1eefa n\u00f3ng ng\u1ecdt ng\u00e0o, b\u00ean tr\u00ean l\u00e0 m\u1ed9t l\u1edbp kem m\u1ecfng nh\u1eb9 t\u1ea1o n\u00ean m\u1ed9t t\u00e1ch c\u00e0 ph\u00ea ho\u00e0n h\u1ea3o v\u1ec1 h\u01b0\u01a1ng v\u1ecb l\u1eabn nh\u00e3n quan.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020035,
                "code": "10020035",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020035
              },
              {
                "product_id": 10020034,
                "code": "10020034",
                "price": 5000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "V\u1eeba",
                "id": 10020034
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/latte_851143_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/latte_851143.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "latte-nong"
      },
      {
        "id": "5b03966a1acd4d5bbd672374",
        "recommended_id": null,
        "name": "Americano \u0110\u00e1",
        "description": "Americano \u0111\u01b0\u1ee3c pha ch\u1ebf b\u1eb1ng c\u00e1ch pha th\u00eam n\u01b0\u1edbc v\u1edbi t\u1ef7 l\u1ec7 nh\u1ea5t \u0111\u1ecbnh v\u00e0o t\u00e1ch c\u00e0 ph\u00ea Espresso, t\u1eeb \u0111\u00f3 mang l\u1ea1i h\u01b0\u01a1ng v\u1ecb nh\u1eb9 nh\u00e0ng v\u00e0 gi\u1eef tr\u1ecdn \u0111\u01b0\u1ee3c m\u00f9i h\u01b0\u01a1ng c\u00e0 ph\u00ea \u0111\u1eb7c tr\u01b0ng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020002,
                "code": "10020002",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020002
              },
              {
                "product_id": 10020001,
                "code": "10020001",
                "price": 5000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "V\u1eeba",
                "id": 10020001
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 24000,
        "base_price_str": "40.000\u0111",
        "price_str": "24.000\u0111",
        "base_price": 40000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/classic-cold-brew_791256_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/classic-cold-brew_791256.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1633143905_ameriacano-tet.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt \u0111\u00e1",
          "kh\u00f4ng \u0111\u01b0\u1eddng"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "americano-da"
      },
      {
        "id": "5b03966a1acd4d5bbd672373",
        "recommended_id": null,
        "name": "Americano N\u00f3ng",
        "description": "Americano \u0111\u01b0\u1ee3c pha ch\u1ebf b\u1eb1ng c\u00e1ch pha th\u00eam n\u01b0\u1edbc v\u1edbi t\u1ef7 l\u1ec7 nh\u1ea5t \u0111\u1ecbnh v\u00e0o t\u00e1ch c\u00e0 ph\u00ea Espresso, t\u1eeb \u0111\u00f3 mang l\u1ea1i h\u01b0\u01a1ng v\u1ecb nh\u1eb9 nh\u00e0ng v\u00e0 gi\u1eef tr\u1ecdn \u0111\u01b0\u1ee3c m\u00f9i h\u01b0\u01a1ng c\u00e0 ph\u00ea \u0111\u1eb7c tr\u01b0ng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020004,
                "code": "10020004",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020004
              },
              {
                "product_id": 10020003,
                "code": "10020003",
                "price": 5000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "V\u1eeba",
                "id": 10020003
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 24000,
        "base_price_str": "40.000\u0111",
        "price_str": "24.000\u0111",
        "base_price": 40000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1636647328_arme-nong_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1636647328_arme-nong.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": ["kh\u00f4ng \u0111\u01b0\u1eddng"],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "americano-nong"
      },
      {
        "id": "5b03966a1acd4d5bbd672378",
        "recommended_id": null,
        "name": "Cappuccino \u0110\u00e1",
        "description": "Capuchino l\u00e0 th\u1ee9c u\u1ed1ng h\u00f2a quy\u1ec7n gi\u1eefa h\u01b0\u01a1ng th\u01a1m c\u1ee7a s\u1eefa, v\u1ecb b\u00e9o c\u1ee7a b\u1ecdt kem c\u00f9ng v\u1ecb \u0111\u1eadm \u0111\u00e0 t\u1eeb c\u00e0 ph\u00ea Espresso. T\u1ea5t c\u1ea3 t\u1ea1o n\u00ean m\u1ed9t h\u01b0\u01a1ng v\u1ecb \u0111\u1eb7c bi\u1ec7t, m\u1ed9t ch\u00fat nh\u1eb9 nh\u00e0ng, tr\u1ea7m l\u1eafng v\u00e0 tinh t\u1ebf.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020016,
                "code": "10020016",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020016
              },
              {
                "product_id": 10020015,
                "code": "10020015",
                "price": 5000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "V\u1eeba",
                "id": 10020015
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/Capu-da_487470_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/Capu-da_487470.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt \u0111\u00e1",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "cappuccino-da"
      },
      {
        "id": "5b03966a1acd4d5bbd672377",
        "recommended_id": null,
        "name": "Cappuccino N\u00f3ng",
        "description": "Capuchino l\u00e0 th\u1ee9c u\u1ed1ng h\u00f2a quy\u1ec7n gi\u1eefa h\u01b0\u01a1ng th\u01a1m c\u1ee7a s\u1eefa, v\u1ecb b\u00e9o c\u1ee7a b\u1ecdt kem c\u00f9ng v\u1ecb \u0111\u1eadm \u0111\u00e0 t\u1eeb c\u00e0 ph\u00ea Espresso. T\u1ea5t c\u1ea3 t\u1ea1o n\u00ean m\u1ed9t h\u01b0\u01a1ng v\u1ecb \u0111\u1eb7c bi\u1ec7t, m\u1ed9t ch\u00fat nh\u1eb9 nh\u00e0ng, tr\u1ea7m l\u1eafng v\u00e0 tinh t\u1ebf.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020018,
                "code": "10020018",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020018
              },
              {
                "product_id": 10020017,
                "code": "10020017",
                "price": 5000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "V\u1eeba",
                "id": 10020017
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/cappuccino_621532_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/cappuccino_621532.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": ["kh\u00f4ng \u0111\u01b0\u1eddng"],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "cappuccino-nong"
      },
      {
        "id": "5b03966a1acd4d5bbd67237f",
        "recommended_id": null,
        "name": "Espresso \u0110\u00e1",
        "description": "M\u1ed9t t\u00e1ch Espresso nguy\u00ean b\u1ea3n \u0111\u01b0\u1ee3c b\u1eaft \u0111\u1ea7u b\u1edfi nh\u1eefng h\u1ea1t Arabica ch\u1ea5t l\u01b0\u1ee3ng, ph\u1ed1i tr\u1ed9n v\u1edbi t\u1ec9 l\u1ec7 c\u00e2n \u0111\u1ed1i h\u1ea1t Robusta, cho ra v\u1ecb ng\u1ecdt caramel, v\u1ecb chua d\u1ecbu v\u00e0 s\u00e1nh \u0111\u1eb7c.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 10020023,
                "code": "10020023",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10020023
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 27000,
        "base_price_str": "45.000\u0111",
        "price_str": "27.000\u0111",
        "base_price": 45000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/cfdenda-espressoDa_185438_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/cfdenda-espressoDa_185438.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "\u00edt \u0111\u00e1",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "espresso-da"
      },
      {
        "id": "5b03966a1acd4d5bbd672380",
        "recommended_id": null,
        "name": "Espresso N\u00f3ng",
        "description": "M\u1ed9t t\u00e1ch Espresso nguy\u00ean b\u1ea3n \u0111\u01b0\u1ee3c b\u1eaft \u0111\u1ea7u b\u1edfi nh\u1eefng h\u1ea1t Arabica ch\u1ea5t l\u01b0\u1ee3ng, ph\u1ed1i tr\u1ed9n v\u1edbi t\u1ec9 l\u1ec7 c\u00e2n \u0111\u1ed1i h\u1ea1t Robusta, cho ra v\u1ecb ng\u1ecdt caramel, v\u1ecb chua d\u1ecbu v\u00e0 s\u00e1nh \u0111\u1eb7c.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020027,
                "code": "10020027",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020027
              },
              {
                "code": "10020026",
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "price": 5000,
                "product_id": 10020026,
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "V\u1eeba",
                "id": 10020026
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 24000,
        "base_price_str": "40.000\u0111",
        "price_str": "24.000\u0111",
        "base_price": 40000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/espressoNong_612688_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/espressoNong_612688.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "espresso-nong"
      },
      {
        "id": "5b03966a1acd4d5bbd672398",
        "recommended_id": null,
        "name": "Mocha \u0110\u00e1",
        "description": "Lo\u1ea1i c\u00e0 ph\u00ea \u0111\u01b0\u1ee3c t\u1ea1o n\u00ean t\u1eeb s\u1ef1 k\u1ebft h\u1ee3p ho\u00e0n h\u1ea3o c\u1ee7a v\u1ecb \u0111\u1eafng \u0111\u1eadm \u0111\u00e0 c\u1ee7a Espresso v\u00e0 s\u1ed1t s\u00f4-c\u00f4-la ng\u1ecdt ng\u00e0o mang t\u1edbi h\u01b0\u01a1ng v\u1ecb \u0111\u1ea7y l\u00f4i cu\u1ed1n, \u0111\u00e1nh th\u1ee9c m\u1ecdi gi\u00e1c quan n\u00ean \u0111\u00e2y ch\u00ednh l\u00e0 th\u1ee9c u\u1ed1ng \u01b0a th\u00edch c\u1ee7a ph\u1ee5 n\u1eef v\u00e0 gi\u1edbi tr\u1ebb.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020037,
                "code": "10020037",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020037
              },
              {
                "product_id": 10020036,
                "code": "10020036",
                "price": 5000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "V\u1eeba",
                "id": 10020036
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/mocha-da_538820_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/mocha-da_538820.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "\u00edt \u0111\u00e1",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "mocha-da"
      },
      {
        "id": "5b03966a1acd4d5bbd672397",
        "recommended_id": null,
        "name": "Mocha N\u00f3ng",
        "description": "Lo\u1ea1i c\u00e0 ph\u00ea \u0111\u01b0\u1ee3c t\u1ea1o n\u00ean t\u1eeb s\u1ef1 k\u1ebft h\u1ee3p ho\u00e0n h\u1ea3o c\u1ee7a v\u1ecb \u0111\u1eafng \u0111\u1eadm \u0111\u00e0 c\u1ee7a Espresso v\u00e0 s\u1ed1t s\u00f4-c\u00f4-la ng\u1ecdt ng\u00e0o mang t\u1edbi h\u01b0\u01a1ng v\u1ecb \u0111\u1ea7y l\u00f4i cu\u1ed1n, \u0111\u00e1nh th\u1ee9c m\u1ecdi gi\u00e1c quan n\u00ean \u0111\u00e2y ch\u00ednh l\u00e0 th\u1ee9c u\u1ed1ng \u01b0a th\u00edch c\u1ee7a ph\u1ee5 n\u1eef v\u00e0 gi\u1edbi tr\u1ebb.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10020040,
                "code": "10020040",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10020040
              },
              {
                "product_id": 10020039,
                "code": "10020039",
                "price": 5000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "V\u1eeba",
                "id": 10020039
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/mochanong_433980_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/mochanong_433980.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "mocha-nong"
      },
      {
        "id": "5ca47f92acf0d3492076b29a",
        "recommended_id": null,
        "name": "Cold Brew Truy\u1ec1n Th\u1ed1ng",
        "description": "T\u1ea1i The Coffee House, Cold Brew \u0111\u01b0\u1ee3c \u1ee7 v\u00e0 ph\u1ee5c v\u1ee5 m\u1ed7i ng\u00e0y t\u1eeb 100% h\u1ea1t Arabica C\u1ea7u \u0110\u1ea5t v\u1edbi h\u01b0\u01a1ng g\u1ed7 th\u00f4ng, h\u1ea1t d\u1ebb, n\u1ed1t s\u00f4-c\u00f4-la \u0111\u1eb7c tr\u01b0ng, thoang tho\u1ea3ng h\u01b0\u01a1ng kh\u00f3i nh\u1eb9 gi\u00fap Cold Brew gi\u1eef nguy\u00ean v\u1ecb t\u01b0\u01a1i m\u1edbi.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 1948,
                "code": "10020230",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 1948
              },
              {
                "code": "10020252",
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "price": 5000,
                "product_id": 10020252,
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "L\u1edbn",
                "id": 10020252
              }
            ]
          }
        ],
        "price": 27000,
        "base_price_str": "45.000\u0111",
        "price_str": "27.000\u0111",
        "base_price": 45000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/classic-cold-brew_239501_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/classic-cold-brew_239501.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "\u00edt \u0111\u00e1",
          "kh\u00f4ng \u0111\u00e1",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "cold-brew-truyen-thong"
      },
      {
        "id": "5ca47f92acf0d3492076b299",
        "recommended_id": null,
        "name": "Cold Brew S\u1eefa T\u01b0\u01a1i",
        "description": "Thanh m\u00e1t v\u00e0 c\u00e2n b\u1eb1ng v\u1edbi h\u01b0\u01a1ng v\u1ecb c\u00e0 ph\u00ea nguy\u00ean b\u1ea3n 100% Arabica C\u1ea7u \u0110\u1ea5t c\u00f9ng s\u1eefa t\u01b0\u01a1i th\u01a1m b\u00e9o cho t\u1eebng ng\u1ee5m tr\u00f2n v\u1ecb, h\u1ea5p d\u1eabn.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 1947,
                "code": "10020229",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 1947
              },
              {
                "code": "10020253",
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "price": 5000,
                "product_id": 10020253,
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "L\u1edbn",
                "id": 10020253
              }
            ]
          }
        ],
        "price": 27000,
        "base_price_str": "45.000\u0111",
        "price_str": "27.000\u0111",
        "base_price": 45000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/cold-brew-sua-tuoi_379576_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/cold-brew-sua-tuoi_379576.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "kh\u00f4ng \u0111\u00e1",
          "\u00edt \u0111\u00e1",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang",
          "nhi\u1ec1u s\u1eefa",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "cold-brew-sua-tuoi"
      },
      {
        "id": "5e4f9e4316bd0a0018d2e24e",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea \u0110\u00e1 Xay-L\u1ea1nh",
        "description": "M\u1ed9t phi\u00ean b\u1ea3n \"upgrade\" t\u1eeb ly c\u00e0 ph\u00ea s\u1eefa quen thu\u1ed9c, nh\u01b0ng l\u1ea1i t\u1ec9nh t\u00e1o v\u00e0 t\u01b0\u01a1i m\u00e1t h\u01a1n b\u1edfi l\u1edbp \u0111\u00e1 xay \u0111i k\u00e8m. Nh\u1ea5p 1 ng\u1ee5m c\u00e0 ph\u00ea \u0111\u00e1 xay l\u00e0 th\u1ea5y \u0111\u00e3, ng\u1ee5m th\u1ee9 hai th\u00eam t\u1ec9nh t\u00e1o v\u00e0 c\u1ee9 th\u1ebf l\u00f4i cu\u1ed1n b\u1ea1n \u0111\u1ebfn ng\u1ee5m cu\u1ed1i c\u00f9ng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10030029",
                "price": 0,
                "product_id": 2198,
                "val": "V\u1eeba",
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2198
              },
              {
                "code": "10030028",
                "price": 6000,
                "product_id": 2199,
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 6.000\u0111",
                "name": "L\u1edbn",
                "id": 2199
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/cf-da-xay-(1)_158038_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/cf-da-xay-(1)_158038.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-da-xay-lanh"
      }
    ]
  },
  {
    "style": 0,
    "id": 5,
    "name": "Tr\u00e0 Tr\u00e1i C\u00e2y - Tr\u00e0 S\u1eefa",
    "title": "Tr\u00e0 Tr\u00e1i C\u00e2y - Tr\u00e0 S\u1eefa",
    "thumbnail": "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/tra-trai-cay-tra-sua.png",
    "slug": "tra-trai-cay-tra-sua",
    "products": [
      {
        "id": "61e00bf0f7d6eb0019d93a50",
        "recommended_id": null,
        "name": "Tr\u00e0 Sen Nh\u00e3n Sum V\u1ea7y",
        "description": "Th\u1ee9c u\u1ed1ng mang h\u01b0\u01a1ng v\u1ecb c\u1ee7a nh\u00e3n, c\u1ee7a sen, c\u1ee7a tr\u00e0 Oolong \u0111\u1ea7y thanh m\u00e1t cho t\u1ea5t c\u1ea3 c\u00e1c th\u00e0nh vi\u00ean trong d\u1ecbp T\u1ebft n\u00e0y. An l\u00e0nh, th\u01b0 th\u00e1i v\u00e0 \u0111\u1eadm \u0111\u00e0 ch\u00ednh l\u00e0 nh\u1eefng g\u00ec The Coffee House mong mu\u1ed1n g\u1eedi trao \u0111\u1ebfn b\u1ea1n v\u00e0 gia \u0111\u00ecnh.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010161",
                "price": 0,
                "product_id": 2659,
                "val": "V\u1eeba",
                "localize": {
                  "en": null,
                  "vi": "Size V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2659
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 1456,
                "product_name": "Extra foam",
                "code": "10100024",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Extra foam",
                "id": 1456
              },
              {
                "product_id": 1667,
                "product_name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "code": "10100016",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "id": 1667
              },
              {
                "product_id": 10100020,
                "product_name": "Sen ng\u00e2m",
                "code": "10100020",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Sen ng\u00e2m",
                "id": 10100020
              },
              {
                "product_id": 10100029,
                "product_name": "Nh\u00e3n ng\u00e2m",
                "code": "10100029",
                "price": 10000,
                "website_name": "Nh\u00e3n ng\u00e2m",
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Nh\u00e3n ng\u00e2m",
                "id": 10100029
              }
            ]
          }
        ],
        "price": 59000,
        "base_price_str": "59.000\u0111",
        "price_str": "59.000\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1642338110_tra-sen-nhan_400x400.jpeg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1642338110_tra-sen-nhan.jpeg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "tra-sen-nhan-sum-vay"
      },
      {
        "id": "61e00bf0f7d6eb0019d93a53",
        "recommended_id": null,
        "name": "Tr\u00e0 D\u01b0a \u0110\u00e0o Sung T\u00fac",
        "description": "V\u1ecb th\u01a1m ng\u1ecdt c\u1ee7a D\u01b0a l\u01b0\u1edbi v\u00e0 \u0111\u00e0o t\u01b0\u01a1i chua chua tr\u00ean n\u1ec1n tr\u00e0 Oolong c\u00f9ng l\u1edbp foam cheese m\u1ecfng nh\u1eb9 t\u1ea1o n\u00ean c\u1ea3m gi\u00e1c sung t\u00fac trong m\u00f9a xu\u00e2n m\u1edbi.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010162",
                "price": 0,
                "product_id": 2655,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2655
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 1456,
                "product_name": "Extra foam",
                "code": "10100024",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Extra foam",
                "id": 1456
              },
              {
                "product_id": 539,
                "product_name": "\u0110\u00e0o ng\u00e2m",
                "code": "10100017",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "\u0110\u00e0o ng\u00e2m",
                "id": 539
              },
              {
                "product_id": 1667,
                "product_name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "code": "10100016",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "id": 1667
              }
            ]
          }
        ],
        "price": 59000,
        "base_price_str": "59.000\u0111",
        "price_str": "59.000\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1642336343_tra-dao-dua-luoi_400x400.jpeg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1642336343_tra-dao-dua-luoi.jpeg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "tra-dua-dao-sung-tuc"
      },
      {
        "id": "61a4f0f461af54001a461a02",
        "recommended_id": null,
        "name": "Tr\u00e0 S\u1eefa Masala Chai Tr\u00e2n Ch\u00e2u (\u0110\u00e1)",
        "description": "S\u1ef1 k\u1ebft h\u1ee3p h\u00e0i h\u00f2a gi\u1eefa tr\u00e0 \u0111en m\u1ea1nh m\u1ebd, s\u1eefa ng\u1ecdt th\u01a1m b\u00e9o v\u1edbi c\u00e1c gia v\u1ecb th\u1ea3o m\u1ed9c \u1ea5m n\u00f3ng t\u1eeb \u1ea4n \u0110\u1ed9, c\u00f3 th\u00eam tr\u00e2n ch\u00e2u mang \u0111\u1ebfn cho b\u1ea1n m\u1ed9t h\u01b0\u01a1ng v\u1ecb tr\u00e0 s\u1eefa \u0111\u1eb7c bi\u1ec7t.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010156",
                "price": 0,
                "product_id": 2633,
                "val": "V\u1eeba",
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2633
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 1456,
                "product_name": "Extra foam",
                "code": "10100024",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Extra foam",
                "id": 1456
              },
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              },
              {
                "product_id": 1667,
                "product_name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "code": "10100016",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "id": 1667
              }
            ]
          }
        ],
        "price": 59000,
        "base_price_str": "59.000\u0111",
        "price_str": "59.000\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1638332240_tra-sua-chai-tran-chau-lanh_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1638332240_tra-sua-chai-tran-chau-lanh.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "tra-sua-masala-chai-tran-chau-da"
      },
      {
        "id": "61a4f0f461af54001a461a01",
        "recommended_id": null,
        "name": "Tr\u00e0 S\u1eefa Masala Chai (N\u00f3ng)",
        "description": "S\u1ef1 k\u1ebft h\u1ee3p h\u00e0i h\u00f2a gi\u1eefa tr\u00e0 \u0111en m\u1ea1nh m\u1ebd, s\u1eefa ng\u1ecdt th\u01a1m b\u00e9o v\u1edbi c\u00e1c gia v\u1ecb th\u1ea3o m\u1ed9c \u1ea5m n\u00f3ng t\u1eeb \u1ea4n \u0110\u1ed9 mang \u0111\u1ebfn cho b\u1ea1n m\u1ed9t h\u01b0\u01a1ng v\u1ecb tr\u00e0 s\u1eefa \u0111\u1eb7c bi\u1ec7t.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010158",
                "price": 0,
                "product_id": 2635,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2635
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 196,
                "product_name": "Espresso (1shot)",
                "code": "10100003",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Espresso (1shot)",
                "id": 196
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1638332229_tra-sua-chai-nong_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1638332229_tra-sua-chai-nong.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-sua-masala-chai-nong"
      },
      {
        "id": "5b03966a1acd4d5bbd67239c",
        "recommended_id": null,
        "name": "Tr\u00e0 \u0110\u00e0o Cam S\u1ea3 - \u0110\u00e1",
        "description": "V\u1ecb thanh ng\u1ecdt c\u1ee7a \u0111\u00e0o, v\u1ecb chua d\u1ecbu c\u1ee7a Cam V\u00e0ng nguy\u00ean v\u1ecf, v\u1ecb ch\u00e1t c\u1ee7a tr\u00e0 \u0111en t\u01b0\u01a1i \u0111\u01b0\u1ee3c \u1ee7 m\u1edbi m\u1ed7i 4 ti\u1ebfng, c\u00f9ng h\u01b0\u01a1ng th\u01a1m n\u1ed3ng \u0111\u1eb7c tr\u01b0ng c\u1ee7a s\u1ea3 ch\u00ednh l\u00e0 \u0111i\u1ec3m s\u00e1ng l\u00e0m n\u00ean s\u1ee9c h\u1ea5p d\u1eabn c\u1ee7a th\u1ee9c u\u1ed1ng n\u00e0y.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "product_id": 10010004,
                "code": "10010004",
                "price": 0,
                "val": "Nh\u1ecf",
                "localize": {
                  "vi": "Nh\u1ecf",
                  "en": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 10010004
              },
              {
                "product_id": 10010003,
                "code": "10010003",
                "price": 7000,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 7.000\u0111",
                "name": "V\u1eeba",
                "id": 10010003
              },
              {
                "code": "10010057",
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "price": 14000,
                "product_id": 10010057,
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 14.000\u0111",
                "name": "L\u1edbn",
                "id": 10010057
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 539,
                "product_name": "\u0110\u00e0o ng\u00e2m",
                "code": "10100017",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "\u0110\u00e0o ng\u00e2m",
                "id": 539
              }
            ]
          }
        ],
        "price": 27000,
        "base_price_str": "45.000\u0111",
        "price_str": "27.000\u0111",
        "base_price": 45000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/tra-dao-cam-xa_668678_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/tra-dao-cam-xa_668678.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "\u00edt \u0111\u00e1",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang",
          "kh\u00f4ng \u0111\u00e1",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-dao-cam-sa-da"
      },
      {
        "id": "5bfb492bacf0d31fd9646728",
        "recommended_id": null,
        "name": "Tr\u00e0 \u0110\u00e0o Cam S\u1ea3 - N\u00f3ng",
        "description": "V\u1ecb thanh ng\u1ecdt c\u1ee7a \u0111\u00e0o, v\u1ecb chua d\u1ecbu c\u1ee7a Cam V\u00e0ng nguy\u00ean v\u1ecf, v\u1ecb ch\u00e1t c\u1ee7a tr\u00e0 \u0111en t\u01b0\u01a1i \u0111\u01b0\u1ee3c \u1ee7 m\u1edbi m\u1ed7i 4 ti\u1ebfng, c\u00f9ng h\u01b0\u01a1ng th\u01a1m n\u1ed3ng \u0111\u1eb7c tr\u01b0ng c\u1ee7a s\u1ea3 ch\u00ednh l\u00e0 \u0111i\u1ec3m s\u00e1ng l\u00e0m n\u00ean s\u1ee9c h\u1ea5p d\u1eabn c\u1ee7a th\u1ee9c u\u1ed1ng n\u00e0y.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 10010042,
                "code": "10010042",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10010042
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 539,
                "product_name": "\u0110\u00e0o ng\u00e2m",
                "code": "10100017",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "\u0110\u00e0o ng\u00e2m",
                "id": 539
              }
            ]
          }
        ],
        "price": 31200,
        "base_price_str": "52.000\u0111",
        "price_str": "31.200\u0111",
        "base_price": 52000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/tdcs-nong_288997_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/tdcs-nong_288997.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-dao-cam-sa-nong"
      },
      {
        "id": "5c3ff3c5acf0d338d22adbae",
        "recommended_id": null,
        "name": "Tr\u00e0 H\u1ea1t Sen - \u0110\u00e1",
        "description": "N\u1ec1n tr\u00e0 oolong h\u1ea3o h\u1ea1ng k\u1ebft h\u1ee3p c\u00f9ng h\u1ea1t sen t\u01b0\u01a1i, b\u00f9i b\u00f9i v\u00e0 l\u1edbp foam cheese b\u00e9o ng\u1eady. Tr\u00e0 h\u1ea1t sen l\u00e0 th\u1ee9c u\u1ed1ng thanh m\u00e1t, nh\u1eb9 nh\u00e0ng ph\u00f9 h\u1ee3p cho c\u1ea3 bu\u1ed5i s\u00e1ng v\u00e0 chi\u1ec1u t\u1ed1i.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "code": "10010043",
                "price": 0,
                "product_id": 1854,
                "val": "Nh\u1ecf",
                "localize": {
                  "en": "Size Nh\u1ecf",
                  "vi": "Size Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 1854
              },
              {
                "code": "10010044",
                "price": 7000,
                "product_id": 1855,
                "val": "V\u1eeba",
                "localize": {
                  "en": "Size V\u1eeba",
                  "vi": "Size V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 7.000\u0111",
                "name": "V\u1eeba",
                "id": 1855
              },
              {
                "code": "10010056",
                "price": 14000,
                "product_id": 1911,
                "val": "L\u1edbn",
                "localize": {
                  "en": "Size L\u1edbn",
                  "vi": "Size L\u1edbn"
                },
                "is_main": true,
                "price_str": "+ 14.000\u0111",
                "name": "L\u1edbn",
                "id": 1911
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 1456,
                "product_name": "Extra foam",
                "code": "10100024",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Extra foam",
                "id": 1456
              },
              {
                "product_id": 10100020,
                "product_name": "Sen ng\u00e2m",
                "code": "10100020",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Sen ng\u00e2m",
                "id": 10100020
              }
            ]
          }
        ],
        "price": 27000,
        "base_price_str": "45.000\u0111",
        "price_str": "27.000\u0111",
        "base_price": 45000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/tra-sen_905594_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/tra-sen_905594.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "\u00edt \u0111\u00e1",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang",
          "kh\u00f4ng \u0111\u00e1",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-hat-sen-da"
      },
      {
        "id": "5c3ff3c5acf0d338d22adbaa",
        "recommended_id": null,
        "name": "Tr\u00e0 H\u1ea1t Sen - N\u00f3ng",
        "description": "N\u1ec1n tr\u00e0 oolong h\u1ea3o h\u1ea1ng k\u1ebft h\u1ee3p c\u00f9ng h\u1ea1t sen t\u01b0\u01a1i, b\u00f9i b\u00f9i th\u01a1m ngon. Tr\u00e0 h\u1ea1t sen l\u00e0 th\u1ee9c u\u1ed1ng thanh m\u00e1t, nh\u1eb9 nh\u00e0ng ph\u00f9 h\u1ee3p cho c\u1ea3 bu\u1ed5i s\u00e1ng v\u00e0 chi\u1ec1u t\u1ed1i.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010048",
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "price": 0,
                "product_id": 1859,
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 1859
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 10100020,
                "product_name": "Sen ng\u00e2m",
                "code": "10100020",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Sen ng\u00e2m",
                "id": 10100020
              }
            ]
          }
        ],
        "price": 31200,
        "base_price_str": "52.000\u0111",
        "price_str": "31.200\u0111",
        "base_price": 52000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/tra-sen-nong_025153_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/tra-sen-nong_025153.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-hat-sen-nong"
      },
      {
        "id": "5b03966a1acd4d5bbd67239e",
        "recommended_id": null,
        "name": "Tr\u00e0 \u0110en Macchiato",
        "description": "Tr\u00e0 \u0111en \u0111\u01b0\u1ee3c \u1ee7 m\u1edbi m\u1ed7i ng\u00e0y, gi\u1eef nguy\u00ean \u0111\u01b0\u1ee3c v\u1ecb ch\u00e1t m\u1ea1nh m\u1ebd \u0111\u1eb7c tr\u01b0ng c\u1ee7a l\u00e1 tr\u00e0, ph\u1ee7 b\u00ean tr\u00ean l\u00e0 l\u1edbp Macchiato \"homemade\" b\u1ed3ng b\u1ec1nh quy\u1ebfn r\u0169 v\u1ecb ph\u00f4 mai m\u1eb7n m\u1eb7n m\u00e0 b\u00e9o b\u00e9o.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010023",
                "price": 0,
                "product_id": 1420,
                "val": "V\u1eeba",
                "localize": {
                  "en": null,
                  "vi": "Size V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 1420
              },
              {
                "code": "10010059",
                "price": 5000,
                "product_id": 1914,
                "val": "L\u1edbn",
                "localize": {
                  "en": null,
                  "vi": "L\u1edbn"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "L\u1edbn",
                "id": 1914
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 1456,
                "product_name": "Extra foam",
                "code": "10100024",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Extra foam",
                "id": 1456
              },
              {
                "product_id": 1667,
                "product_name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "code": "10100016",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "id": 1667
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/tra-den-matchiato_430281_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/tra-den-matchiato_430281.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "kh\u00f4ng kem",
          "\u00edt \u0111\u00e1",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-den-macchiato"
      },
      {
        "id": "6014df0e540c0c001894d83c",
        "recommended_id": null,
        "name": "H\u1ed3ng Tr\u00e0 S\u1eefa Tr\u00e2n Ch\u00e2u",
        "description": "Th\u00eam ch\u00fat ng\u1ecdt ng\u00e0o cho ng\u00e0y m\u1edbi v\u1edbi h\u1ed3ng tr\u00e0 nguy\u00ean l\u00e1, s\u1eefa th\u01a1m ng\u1eady \u0111\u01b0\u1ee3c c\u00e2n ch\u1ec9nh v\u1edbi t\u1ec9 l\u1ec7 ho\u00e0n h\u1ea3o, c\u00f9ng tr\u00e2n ch\u00e2u tr\u1eafng dai gi\u00f2n c\u00f3 s\u1eb5n \u0111\u1ec3 b\u1ea1n t\u1eadn h\u01b0\u1edfng t\u1eebng ng\u1ee5m tr\u00e0 s\u1eefa ng\u1ecdt ng\u00e0o th\u01a1m ng\u1eady thi\u1ec7t \u0111\u00e3.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010134",
                "price": 0,
                "product_id": 2520,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2520
              },
              {
                "code": "10010138",
                "price": 5000,
                "product_id": 2529,
                "val": "L\u1edbn",
                "localize": {
                  "en": null,
                  "vi": "L\u1edbn"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "L\u1edbn",
                "id": 2529
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 1667,
                "product_name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "code": "10100016",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "id": 1667
              }
            ]
          }
        ],
        "price": 33000,
        "base_price_str": "55.000\u0111",
        "price_str": "33.000\u0111",
        "base_price": 55000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/hong-tra-sua-tran-chau_326977_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/hong-tra-sua-tran-chau_326977.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "\u00edt \u0111\u00e1",
          "kh\u00f4ng tr\u00e2n ch\u00e2u",
          "kh\u00f4ng \u0111\u00e1"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "hong-tra-sua-tran-chau"
      },
      {
        "id": "6014df0e540c0c001894d83d",
        "recommended_id": null,
        "name": "H\u1ed3ng Tr\u00e0 S\u1eefa N\u00f3ng",
        "description": "T\u1eebng ng\u1ee5m tr\u00e0 chu\u1ea9n gu \u1ea5m \u00e1p, \u0111\u1eadm \u0111\u00e0 beo b\u00e9o b\u1edfi l\u1edbp s\u1eefa t\u01b0\u01a1i ch\u00e2n \u00e1i ho\u00e0 quy\u1ec7n.\n\nTr\u00e0 \u0111en nguy\u00ean l\u00e1 \u00e2m \u1ea5m d\u1ecbu nh\u1eb9, quy\u1ec7n c\u00f9ng l\u1edbp s\u1eefa th\u01a1m b\u00e9o kh\u00f3 l\u1eabn - h\u01b0\u01a1ng v\u1ecb \u1ea5m \u00e1p chu\u1ea9n gu tr\u00e0, cho t\u1eebng ng\u1ee5m nh\u1eb9 nh\u00e0ng, ng\u1ecdt d\u1ecbu l\u01b0u luy\u1ebfn m\u00e3i n\u01a1i cu\u1ed1ng h\u1ecdng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010135",
                "price": 0,
                "product_id": 2519,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2519
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/hong-tra-sua-nong_941687_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/hong-tra-sua-nong_941687.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": ["\u00edt ng\u1ecdt"],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "hong-tra-sua-nong"
      },
      {
        "id": "60c62c8215234d0011ede4a2",
        "recommended_id": null,
        "name": "Tr\u00e0 s\u1eefa Oolong N\u01b0\u1edbng Tr\u00e2n Ch\u00e2u",
        "description": "H\u01b0\u01a1ng v\u1ecb ch\u00e2n \u00e1i \u0111\u00fang gu \u0111\u1eadm \u0111\u00e0 v\u1edbi tr\u00e0 oolong \u0111\u01b0\u1ee3c \u201csao\u201d (n\u01b0\u1edbng) l\u00e2u h\u01a1n cho h\u01b0\u01a1ng v\u1ecb \u0111\u1eadm \u0111\u00e0, h\u00f2a quy\u1ec7n v\u1edbi s\u1eefa th\u01a1m b\u00e9o mang \u0111\u1ebfn c\u1ea3m gi\u00e1c m\u00e1t l\u1ea1nh, l\u01b0u luy\u1ebfn v\u1ecb tr\u00e0 s\u1eefa \u0111\u1eadm \u0111\u00e0 n\u01a1i v\u00f2m h\u1ecdng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010148",
                "price": 0,
                "product_id": 2559,
                "val": "V\u1eeba",
                "localize": {
                  "en": null,
                  "vi": "Size V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2559
              },
              {
                "code": "10010150",
                "price": 5000,
                "product_id": 2560,
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "L\u1edbn",
                "id": 2560
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 1667,
                "product_name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "code": "10100016",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "id": 1667
              }
            ]
          }
        ],
        "price": 33000,
        "base_price_str": "55.000\u0111",
        "price_str": "33.000\u0111",
        "base_price": 55000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/olong-nuong-tran-chau_017573_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/olong-nuong-tran-chau_017573.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-sua-oolong-nuong-tran-chau"
      },
      {
        "id": "60c62c8215234d0011ede4a1",
        "recommended_id": null,
        "name": "Tr\u00e0 s\u1eefa Oolong N\u01b0\u1edbng (N\u00f3ng)",
        "description": "\u0110\u1eadm \u0111\u00e0 chu\u1ea9n gu v\u00e0 \u1ea5m n\u00f3ng - b\u1edfi l\u1edbp tr\u00e0 oolong n\u01b0\u1edbng \u0111\u1eadm v\u1ecb ho\u00e0 c\u00f9ng l\u1edbp s\u1eefa th\u01a1m b\u00e9o. H\u01b0\u01a1ng v\u1ecb ch\u00e2n \u00e1i \u0111\u00fang gu \u0111\u1eadm \u0111\u00e0 - tr\u00e0 oolong \u0111\u01b0\u1ee3c \"sao\" (n\u01b0\u1edbng) l\u00e2u h\u01a1n cho v\u1ecb \u0111\u1eadm \u0111\u00e0, ho\u00e0 quy\u1ec7n v\u1edbi s\u1eefa th\u01a1m ng\u1eady. Cho t\u1eebng ng\u1ee5m \u1ea5m \u00e1p, l\u01b0u luy\u1ebfn v\u1ecb tr\u00e0 s\u1eefa \u0111\u1eadm \u0111\u00e0 m\u00e3i n\u01a1i cu\u1ed1ng h\u1ecdng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010149",
                "price": 0,
                "product_id": 2561,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2561
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/oolong-nuong-nong_948581_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/oolong-nuong-nong_948581.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-sua-oolong-nuong-nong"
      },
      {
        "id": "5e92fd7dc788bc0011abaa06",
        "recommended_id": null,
        "name": "Tr\u00e0 S\u1eefa M\u1eafc Ca Tr\u00e2n Ch\u00e2u",
        "description": "M\u1ed7i ng\u00e0y v\u1edbi The Coffee House s\u1ebd l\u00e0 \u0111i\u1ec1u t\u01b0\u01a1i m\u1edbi h\u01a1n v\u1edbi s\u1eefa h\u1ea1t m\u1eafc ca th\u01a1m ngon, b\u1ed5 d\u01b0\u1ee1ng quy\u1ec7n c\u00f9ng n\u1ec1n tr\u00e0 oolong cho v\u1ecb c\u00e2n b\u1eb1ng, ng\u1ecdt d\u1ecbu \u0111i k\u00e8m c\u00f9ng Tr\u00e2n ch\u00e2u tr\u1eafng gi\u00f2n dai mang l\u1ea1i c\u1ea3m gi\u00e1c \u201c\u0111\u00e3\u201d trong t\u1eebng ng\u1ee5m tr\u00e0 s\u1eefa.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010092",
                "price": 0,
                "product_id": 2215,
                "val": "V\u1eeba",
                "localize": {
                  "en": null,
                  "vi": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2215
              },
              {
                "code": "10010091",
                "price": 5000,
                "product_id": 2216,
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "L\u1edbn",
                "id": 2216
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 1667,
                "product_name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "code": "10100016",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Tr\u00e2n ch\u00e2u tr\u1eafng",
                "id": 1667
              }
            ]
          }
        ],
        "price": 30000,
        "base_price_str": "50.000\u0111",
        "price_str": "30.000\u0111",
        "base_price": 50000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/tra-sua-mac-ca_377522_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/tra-sua-mac-ca_377522.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "\u00edt \u0111\u00e1",
          "kh\u00f4ng tr\u00e2n ch\u00e2u",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-sua-mac-ca-tran-chau"
      },
      {
        "id": "605da09f717e5d00114a3cdc",
        "recommended_id": null,
        "name": "H\u1ed3ng Tr\u00e0 Latte Macchiato",
        "description": "S\u1ef1 k\u1ebft h\u1ee3p ho\u00e0n h\u1ea3o b\u1edfi h\u1ed3ng tr\u00e0 d\u1ecbu nh\u1eb9 v\u00e0 s\u1eefa t\u01b0\u01a1i, nh\u1ea5n nh\u00e1 th\u00eam l\u1edbp macchiato tr\u1ee9 danh c\u1ee7a The Coffee House mang \u0111\u1ebfn cho b\u1ea1n h\u01b0\u01a1ng v\u1ecb tr\u00e0 s\u1eefa \u0111\u00fang gu tinh t\u1ebf v\u00e0 healthy.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010139",
                "price": 0,
                "product_id": 2531,
                "val": "V\u1eeba",
                "localize": {
                  "en": null,
                  "vi": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2531
              },
              {
                "code": "10010140",
                "price": 5000,
                "product_id": 2532,
                "val": "L\u1edbn",
                "localize": {
                  "en": null,
                  "vi": "L\u1edbn"
                },
                "is_main": true,
                "price_str": "+ 5.000\u0111",
                "name": "L\u1edbn",
                "id": 2532
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 1456,
                "product_name": "Extra foam",
                "code": "10100024",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Extra foam",
                "id": 1456
              }
            ]
          }
        ],
        "price": 33000,
        "base_price_str": "55.000\u0111",
        "price_str": "33.000\u0111",
        "base_price": 55000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/hong-tra-latte_618293_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/hong-tra-latte_618293.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "\u00edt \u0111\u00e1",
          "kh\u00f4ng \u0111\u00e1",
          "\u0111\u00e1 \u0111\u1ec3 ri\u00eang"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "hong-tra-latte-macchiato"
      },
      {
        "id": "61534bde26ae260012abe218",
        "recommended_id": null,
        "name": "Tr\u00e0 \u0110\u00e0o Cam S\u1ea3 Chai Fresh 500ML",
        "description": "V\u1edbi phi\u00ean b\u1ea3n chai fresh 500ml, th\u1ee9c u\u1ed1ng \"best seller\" \u0111\u1ec9nh cao mang m\u1ed9t di\u1ec7n m\u1ea1o t\u01b0\u01a1i m\u1edbi, ti\u1ec7n l\u1ee3i, ph\u00f9 h\u1ee3p v\u1edbi b\u00ecnh th\u01b0\u1eddng m\u1edbi v\u00e0 v\u1eabn gi\u1eef nguy\u00ean v\u1ecb thanh ng\u1ecdt c\u1ee7a \u0111\u00e0o, v\u1ecb chua d\u1ecbu c\u1ee7a cam v\u00e0ng nguy\u00ean v\u1ecf v\u00e0 v\u1ecb tr\u00e0 \u0111en th\u01a1m l\u1eebng ly Tr\u00e0 \u0111\u00e0o cam s\u1ea3 nguy\u00ean b\u1ea3n.\n*S\u1ea3n ph\u1ea9m d\u00f9ng ngon nh\u1ea5t trong ng\u00e0y.\n*S\u1ea3n ph\u1ea9m m\u1eb7c \u0111\u1ecbnh m\u1ee9c \u0111\u01b0\u1eddng v\u00e0 kh\u00f4ng \u0111\u00e1.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010151",
                "price": 0,
                "product_id": 2605,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2605
              }
            ]
          }
        ],
        "price": 65400,
        "base_price_str": "109.000\u0111",
        "price_str": "65.400\u0111",
        "base_price": 109000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/Bottle_TraDao_836487_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/Bottle_TraDao_836487.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1633147050_photo-2021-10-02-10-52-44.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1633147051_photo-2021-10-02-10-52-45.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1633147051_photo-2021-10-02-10-56-21.jpg"
        ],
        "is_pickup": false,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-dao-cam-sa-chai-fresh-500ml"
      },
      {
        "id": "61a4f0f461af54001a461a00",
        "recommended_id": null,
        "name": "Tr\u00e0 S\u1eefa Masala Chai Tr\u00e2n Ch\u00e2u Chai Fresh 500ml",
        "description": "Phi\u00ean b\u1ea3n Chai Fresh ti\u1ec7n l\u1ee3i v\u1edbi th\u1ee9c u\u1ed1ng m\u1edbi l\u1ea1, ngon l\u00e0nh v\u00e0 t\u1ed1t cho s\u1ee9c kh\u1ecfe s\u1ebd mang \u0111\u1ebfn cho b\u1ea1n tr\u1ea3i nghi\u1ec7m t\u1ed1t h\u01a1n bao gi\u1edd h\u1ebft.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010159",
                "price": 0,
                "product_id": 2636,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2636
              }
            ]
          }
        ],
        "price": 65400,
        "base_price_str": "109.000\u0111",
        "price_str": "65.400\u0111",
        "base_price": 109000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1638596210_bottle-masalachai-new_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1638596210_bottle-masalachai-new.jpg"
        ],
        "is_pickup": false,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-sua-masala-chai-tran-chau-chai-fresh-500ml"
      },
      {
        "id": "605da09f717e5d00114a3cd8",
        "recommended_id": null,
        "name": "Tr\u00e0 Long Nh\u00e3n H\u1ea1t Chia",
        "description": "V\u1ecb nh\u00e3n ng\u1ecdt, t\u01b0\u01a1i m\u00e1t \u0111\u1eb7c tr\u01b0ng ho\u00e0 quy\u1ec7n tinh t\u1ebf c\u00f9ng v\u1ecb tr\u00e0 oolong h\u1ea3o h\u1ea1ng v\u00e0 h\u1ea1t chia mang \u0111\u1ebfn cho b\u1ea1n m\u1ed9t th\u1ee9c u\u1ed1ng kh\u00f4ng ch\u1ec9 th\u01a1m ngon m\u00e0 c\u00f2n b\u1ed5 d\u01b0\u1ee1ng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 1,
            "items": [
              {
                "code": "10010145",
                "price": 0,
                "product_id": 2537,
                "val": "Nh\u1ecf",
                "localize": {
                  "en": null,
                  "vi": "Nh\u1ecf"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Nh\u1ecf",
                "id": 2537
              },
              {
                "code": "10010146",
                "price": 7000,
                "product_id": 2538,
                "val": "V\u1eeba",
                "localize": {
                  "en": null,
                  "vi": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 7.000\u0111",
                "name": "V\u1eeba",
                "id": 2538
              },
              {
                "code": "10010147",
                "price": 14000,
                "product_id": 2539,
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 14.000\u0111",
                "name": "L\u1edbn",
                "id": 2539
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 10100029,
                "product_name": "Nh\u00e3n ng\u00e2m",
                "code": "10100029",
                "price": 10000,
                "website_name": "Nh\u00e3n ng\u00e2m",
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Nh\u00e3n ng\u00e2m",
                "id": 10100029
              }
            ]
          }
        ],
        "price": 27000,
        "base_price_str": "45.000\u0111",
        "price_str": "27.000\u0111",
        "base_price": 45000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/tra-nhan-da_484810_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/tra-nhan-da_484810.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": ["\u00edt ng\u1ecdt"],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-long-nhan-hat-chia"
      },
      {
        "id": "605da09f717e5d00114a3cd9",
        "recommended_id": null,
        "name": "Tr\u00e0 Long Nh\u00e3n H\u1ea1t Chia (N\u00f3ng)",
        "description": "V\u1ecb nh\u00e3n ng\u1ecdt, t\u01b0\u01a1i m\u00e1t \u0111\u1eb7c tr\u01b0ng ho\u00e0 quy\u1ec7n tinh t\u1ebf c\u00f9ng v\u1ecb tr\u00e0 oolong h\u1ea3o h\u1ea1ng v\u00e0 h\u1ea1t chia mang \u0111\u1ebfn cho b\u1ea1n m\u1ed9t th\u1ee9c u\u1ed1ng kh\u00f4ng ch\u1ec9 th\u01a1m ngon m\u00e0 c\u00f2n b\u1ed5 d\u01b0\u1ee1ng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010144",
                "price": 0,
                "product_id": 2536,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2536
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 10100029,
                "product_name": "Nh\u00e3n ng\u00e2m",
                "code": "10100029",
                "price": 10000,
                "website_name": "Nh\u00e3n ng\u00e2m",
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Nh\u00e3n ng\u00e2m",
                "id": 10100029
              }
            ]
          }
        ],
        "price": 31200,
        "base_price_str": "52.000\u0111",
        "price_str": "31.200\u0111",
        "base_price": 52000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/nhan-hat-chia--nong_140427_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/nhan-hat-chia--nong_140427.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": ["\u00edt ng\u1ecdt"],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-long-nhan-hat-chia-nong"
      },
      {
        "id": "61534bde26ae260012abe217",
        "recommended_id": null,
        "name": "Tr\u00e0 S\u1eefa Oolong N\u01b0\u1edbng Tr\u00e2n Ch\u00e2u Chai Fresh 500ML",
        "description": "Phi\u00ean b\u1ea3n chai fresh 500ml m\u1edbi, The Coffee House tin r\u1eb1ng v\u1edbi di\u1ec7n m\u1ea1o m\u1edbi: ti\u1ec7n l\u1ee3i v\u00e0 ph\u00f9 h\u1ee3p v\u1edbi b\u00ecnh th\u01b0\u1eddng m\u1edbi n\u00e0y, c\u00e1c t\u00edn \u0111\u1ed3 tr\u00e0 s\u1eefa s\u1ebd \u0111\u01b0\u1ee3c th\u01b0\u1edfng th\u1ee9c h\u01b0\u01a1ng v\u1ecb \u0111\u1eadm \u0111\u00e0, h\u00f2a quy\u1ec7n v\u1edbi s\u1eefa th\u01a1m b\u00e9o mang \u0111\u1ebfn c\u1ea3m gi\u00e1c m\u00e1t l\u1ea1nh \u1edf b\u1ea5t c\u1ee9 n\u01a1i \u0111\u00e2u.\n*S\u1ea3n ph\u1ea9m d\u00f9ng ngon nh\u1ea5t trong ng\u00e0y.\n*S\u1ea3n ph\u1ea9m m\u1eb7c \u0111\u1ecbnh m\u1ee9c \u0111\u01b0\u1eddng v\u00e0 kh\u00f4ng \u0111\u00e1.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10010152",
                "price": 0,
                "product_id": 2606,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2606
              }
            ]
          }
        ],
        "price": 59400,
        "base_price_str": "99.000\u0111",
        "price_str": "59.400\u0111",
        "base_price": 99000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/Bottle_oolong_285082_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/Bottle_oolong_285082.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1633147069_photo-2021-10-02-10-56-55.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1633147069_photo-2021-10-02-10-57-00.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1633147069_photo-2021-10-02-10-56-49.jpg"
        ],
        "is_pickup": false,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-sua-oolong-nuong-tran-chau-chai-fresh-500ml"
      }
    ]
  },
  {
    "style": 0,
    "id": 2,
    "name": "\u0110\u00e1 Xay - Choco - Matcha",
    "title": "\u0110\u00e1 Xay - Choco - Matcha",
    "thumbnail": "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/da-xa.png",
    "slug": "da-xay-chocolate-matcha",
    "products": [
      {
        "id": "5e4f9e4316bd0a0018d2e24e",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea \u0110\u00e1 Xay-L\u1ea1nh",
        "description": "M\u1ed9t phi\u00ean b\u1ea3n \"upgrade\" t\u1eeb ly c\u00e0 ph\u00ea s\u1eefa quen thu\u1ed9c, nh\u01b0ng l\u1ea1i t\u1ec9nh t\u00e1o v\u00e0 t\u01b0\u01a1i m\u00e1t h\u01a1n b\u1edfi l\u1edbp \u0111\u00e1 xay \u0111i k\u00e8m. Nh\u1ea5p 1 ng\u1ee5m c\u00e0 ph\u00ea \u0111\u00e1 xay l\u00e0 th\u1ea5y \u0111\u00e3, ng\u1ee5m th\u1ee9 hai th\u00eam t\u1ec9nh t\u00e1o v\u00e0 c\u1ee9 th\u1ebf l\u00f4i cu\u1ed1n b\u1ea1n \u0111\u1ebfn ng\u1ee5m cu\u1ed1i c\u00f9ng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10030029",
                "price": 0,
                "product_id": 2198,
                "val": "V\u1eeba",
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2198
              },
              {
                "code": "10030028",
                "price": 6000,
                "product_id": 2199,
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 6.000\u0111",
                "name": "L\u1edbn",
                "id": 2199
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/cf-da-xay-(1)_158038_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/cf-da-xay-(1)_158038.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-da-xay-lanh"
      },
      {
        "id": "5cdb677aacf0d33b682b4b73",
        "recommended_id": null,
        "name": "Chanh S\u1ea3 \u0110\u00e1 Xay",
        "description": "S\u1ef1 k\u1ebft h\u1ee3p h\u00e0i ho\u00e0 gi\u1eefa Chanh & s\u1ea3 - nh\u1eefng nguy\u00ean li\u1ec7u m\u1ed9c m\u1ea1c r\u1ea5t \u0111\u1ed7i quen thu\u1ed9c cho ra \u0111\u1eddi m\u1ed9t th\u1ee9c u\u1ed1ng thanh m\u00e1t, v\u1ecb chua chua ng\u1ecdt ng\u1ecdt gi\u1ea3i nhi\u1ec7t l\u1ea1i t\u1ed1t cho s\u1ee9c kh\u1ecfe.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 1968,
                "code": "10030011",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 1968
              },
              {
                "code": "10030017",
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "price": 6000,
                "product_id": 10030017,
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 6.000\u0111",
                "name": "L\u1edbn",
                "id": 10030017
              }
            ]
          }
        ],
        "price": 29400,
        "base_price_str": "49.000\u0111",
        "price_str": "29.400\u0111",
        "base_price": 49000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/Chanh-sa-da-xay_170980_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/Chanh-sa-da-xay_170980.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "chanh-sa-da-xay"
      },
      {
        "id": "5b03966a1acd4d5bbd67237e",
        "recommended_id": null,
        "name": "Cookie \u0110\u00e1 Xay",
        "description": "Nh\u1eefng m\u1ea9u b\u00e1nh cookies gi\u00f2n r\u1ee5m k\u1ebft h\u1ee3p \u0103n \u00fd v\u1edbi s\u1eefa t\u01b0\u01a1i, kem t\u01b0\u01a1i b\u00e9o ng\u1ecdt v\u00e0 \u0111\u00e1 xay m\u00e1t l\u00e0nh, \u0111em \u0111\u1ebfn c\u1ea3m gi\u00e1c l\u1ea1 mi\u1ec7ng g\u00e2y th\u00edch th\u00fa v\u00e0 kh\u00f4ng th\u1ec3 c\u01b0\u1ee1ng l\u1ea1i. M\u1ed9t m\u00f3n u\u1ed1ng ph\u00e1 c\u00e1ch d\u1ec5 th\u01b0\u01a1ng \u0111\u1ea7y m\u00ea ho\u1eb7c.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 10030003,
                "code": "10030003",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10030003
              },
              {
                "code": "10030018",
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "price": 6000,
                "product_id": 10030018,
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 6.000\u0111",
                "name": "L\u1edbn",
                "id": 10030018
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1638170021_cookie-da-xay_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1638170021_cookie-da-xay.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "cookie-da-xay"
      },
      {
        "id": "5b03966a1acd4d5bbd67237c",
        "recommended_id": null,
        "name": "Chocolate \u0110\u00e1 Xay",
        "description": "S\u1eefa v\u00e0 kem t\u01b0\u01a1i b\u00e9o ng\u1ecdt \u0111\u01b0\u1ee3c \u201cc\u00e1 t\u00ednh ho\u00e1\u201d b\u1edfi v\u1ecb chocolate/s\u00f4-c\u00f4-la \u0111\u0103ng \u0111\u1eafng. D\u00e0nh cho c\u00e1c t\u00edn \u0111\u1ed3 h\u1ea3o ng\u1ecdt. L\u1ef1a ch\u1ecdn h\u00e0ng \u0111\u1ea7u n\u1ebfu b\u1ea1n \u0111ang c\u1ea7n ch\u00fat n\u0103ng l\u01b0\u1ee3ng tinh th\u1ea7n.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 10030002,
                "code": "10030002",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10030002
              },
              {
                "code": "10030019",
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "price": 6000,
                "product_id": 10030019,
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 6.000\u0111",
                "name": "L\u1edbn",
                "id": 10030019
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 10100018,
                "product_name": "Sauce Chocolate",
                "code": "10100018",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Sauce Chocolate",
                "id": 10100018
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/Chocolate-ice-blended_400940_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/Chocolate-ice-blended_400940.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "chocolate-da-xay"
      },
      {
        "id": "5d2bdfa5acf0d30ba264432b",
        "recommended_id": null,
        "name": "\u0110\u00e0o Vi\u1ec7t Qu\u1ea5t \u0110\u00e1 Xay",
        "description": "V\u1ecb \u0111\u00e0o quen thu\u1ed9c, \u0111\u01b0\u1ee3c kho\u00e1c l\u00ean m\u00ecnh d\u00e1ng v\u1ebb thanh m\u00e1t h\u01a1n khi k\u1ebft h\u1ee3p v\u1edbi m\u1ee9t berry v\u00e0 l\u1edbp kem ng\u1ecdt b\u00e9o ng\u1eady, cho h\u01b0\u01a1ng v\u1ecb k\u00edch th\u00edch v\u1ecb gi\u00e1c \u0111\u1ea7y l\u00f4i cu\u1ed1n v\u00e0 khoan kho\u00e1i ngay t\u1eeb ng\u1ee5m \u0111\u1ea7u ti\u00ean.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 2002,
                "code": "10030014",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2002
              },
              {
                "code": "10030015",
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "price": 6000,
                "product_id": 2003,
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 6.000\u0111",
                "name": "L\u1edbn",
                "id": 2003
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/DaoVietQuat_033985_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/DaoVietQuat_033985.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "dao-viet-quat-da-xay"
      },
      {
        "id": "5ffbb2671327f700184f54d4",
        "recommended_id": null,
        "name": "Yogurt D\u01b0a L\u01b0\u1edbi ph\u00e1t t\u00e0i",
        "description": "V\u1ecb yogurt chua ng\u1ecdt, m\u00e1t l\u1ea1nh t\u00e1i t\u00ea, th\u00eam topping d\u01b0a l\u01b0\u1edbi v\u00e0ng t\u01b0\u01a1i, th\u01a1m l\u1eebng, vui mi\u1ec7ng. Th\u1eadt kh\u00f3 \u0111\u1ec3 kh\u00f4ng y\u00eau ngay t\u1eeb ng\u1ee5m \u0111\u1ea7u ti\u00ean.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10040033",
                "price": 0,
                "product_id": 2509,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2509
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/tra-dua-luoi_114296_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/tra-dua-luoi_114296.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "yogurt-dua-luoi-phat-tai"
      },
      {
        "id": "5b03966a1acd4d5bbd672393",
        "recommended_id": null,
        "name": "Matcha \u0110\u00e1 Xay",
        "description": "Matcha thanh, nh\u1eabn, v\u00e0 \u0111\u1eafng nh\u1eb9 \u0111\u01b0\u1ee3c nh\u00e2n \u0111\u00f4i s\u1ea3ng kho\u00e1i khi u\u1ed1ng l\u1ea1nh. Nh\u1ea5n nh\u00e1 th\u00eam nh\u1eefng n\u00e9t b\u00f9i b\u00e9o c\u1ee7a kem v\u00e0 s\u1eefa. G\u00e2y th\u01b0\u01a1ng nh\u1edb v\u00f4 c\u00f9ng!",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 10030005,
                "code": "10030005",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10030005
              },
              {
                "code": "10030016",
                "localize": {
                  "en": "L\u1edbn",
                  "vi": "L\u1edbn"
                },
                "price": 6000,
                "product_id": 10030016,
                "val": "L\u1edbn",
                "is_main": true,
                "price_str": "+ 6.000\u0111",
                "name": "L\u1edbn",
                "id": 10030016
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/matchadaxay_622077_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/matchadaxay_622077.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "matcha-da-xay"
      },
      {
        "id": "5b03966a1acd4d5bbd672375",
        "recommended_id": null,
        "name": "Sinh T\u1ed1 Vi\u1ec7t Qu\u1ea5t",
        "description": "M\u1ee9t Vi\u1ec7t Qu\u1ea5t chua thanh, ng\u00f2n ng\u1ecdt, ph\u1ed1i h\u1ee3p nh\u1ecbp nh\u00e0ng v\u1edbi d\u00f2ng s\u1eefa chua b\u1ed5 d\u01b0\u1ee1ng. L\u00e0 m\u00f3n sinh t\u1ed1 th\u01a1m ngon m\u00e0 c\u1ea3 \u0111\u1ea7u l\u01b0\u1ee1i v\u00e0 l\u00e0n da \u0111\u1ec1u th\u00edch.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 10040007,
                "code": "10040007",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10040007
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/sinh-to-viet-quoc_145138_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/sinh-to-viet-quoc_145138.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "kh\u00f4ng \u0111\u01b0\u1eddng",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "sinh-to-viet-quat"
      },
      {
        "id": "5b03966a1acd4d5bbd672394",
        "recommended_id": null,
        "name": "Tr\u00e0 Matcha Latte N\u00f3ng",
        "description": "V\u1edbi m\u00e0u xanh m\u00e1t m\u1eaft c\u1ee7a b\u1ed9t tr\u00e0 Matcha, v\u1ecb ng\u1ecdt nh\u1eb9 nh\u00e0ng, pha tr\u1ed9n c\u00f9ng s\u1eefa t\u01b0\u01a1i v\u00e0 l\u1edbp foam m\u1ec1m m\u1ecbn, Matcha Latte s\u1ebd khi\u1ebfn b\u1ea1n y\u00eau ngay t\u1eeb l\u1ea7n \u0111\u1ea7u ti\u00ean.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 10010015,
                "code": "10010015",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10010015
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/matcha-latte-_936022_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/matcha-latte-_936022.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-matcha-latte-nong"
      },
      {
        "id": "5b03966a1acd4d5bbd672395",
        "recommended_id": null,
        "name": "Tr\u00e0 Matcha Latte \u0110\u00e1",
        "description": "V\u1edbi m\u00e0u xanh m\u00e1t m\u1eaft c\u1ee7a b\u1ed9t tr\u00e0 Matcha, v\u1ecb ng\u1ecdt nh\u1eb9 nh\u00e0ng, pha tr\u1ed9n c\u00f9ng s\u1eefa t\u01b0\u01a1i v\u00e0 l\u1edbp foam m\u1ec1m m\u1ecbn, Matcha Latte s\u1ebd khi\u1ebfn b\u1ea1n y\u00eau ngay t\u1eeb l\u1ea7n \u0111\u1ea7u ti\u00ean.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 10010014,
                "code": "10010014",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": null
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10010014
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/matcha-latte-da_083173_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/matcha-latte-da_083173.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "\u00edt ng\u1ecdt",
          "Kh\u00f4ng l\u1ea5y \u1ed1ng h\u00fat & mu\u1ed7ng nh\u1ef1a"
        ],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "tra-matcha-latte-da"
      },
      {
        "id": "5b03966a1acd4d5bbd67239d",
        "recommended_id": null,
        "name": "Chocolate \u0110\u00e1",
        "description": "B\u1ed9t chocolate nguy\u00ean ch\u1ea5t ho\u00e0 c\u00f9ng s\u1eefa t\u01b0\u01a1i b\u00e9o ng\u1eady. V\u1ecb ng\u1ecdt t\u1ef1 nhi\u00ean, kh\u00f4ng g\u1eaft c\u1ed5, \u0111\u1ec3 l\u1ea1i m\u1ed9t ch\u00fat \u0111\u1eafng nh\u1eb9, cay cay tr\u00ean \u0111\u1ea7u l\u01b0\u1ee1i.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": 10050004,
                "code": "10050004",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": "V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 10050004
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 10100018,
                "product_name": "Sauce Chocolate",
                "code": "10100018",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Sauce Chocolate",
                "id": 10100018
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/chocolate-da_877186_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/chocolate-da_877186.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "chocolate-da"
      },
      {
        "id": "5b4c2f58a9d0590cb37cdade",
        "recommended_id": null,
        "name": "Chocolate N\u00f3ng",
        "description": "B\u1ed9t chocolate nguy\u00ean ch\u1ea5t ho\u00e0 c\u00f9ng s\u1eefa t\u01b0\u01a1i b\u00e9o ng\u1eady. V\u1ecb ng\u1ecdt t\u1ef1 nhi\u00ean, kh\u00f4ng g\u1eaft c\u1ed5, \u0111\u1ec3 l\u1ea1i m\u1ed9t ch\u00fat \u0111\u1eafng nh\u1eb9, cay cay tr\u00ean \u0111\u1ea7u l\u01b0\u1ee1i.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": "41",
                "code": "10050003",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "V\u1eeba",
                  "en": null
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 41
              }
            ]
          },
          {
            "group_id": 1,
            "name": "Topping",
            "description": "Ch\u1ecdn t\u1ed1i \u0111a 2 lo\u1ea1i",
            "min": 0,
            "max": 2,
            "default_index": -1,
            "items": [
              {
                "product_id": 10100018,
                "product_name": "Sauce Chocolate",
                "code": "10100018",
                "price": 10000,
                "type": 3,
                "price_str": "+ 10.000\u0111",
                "name": "Sauce Chocolate",
                "id": 10100018
              }
            ]
          }
        ],
        "price": 35400,
        "base_price_str": "59.000\u0111",
        "price_str": "35.400\u0111",
        "base_price": 59000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/chocolatenong_949029_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/chocolatenong_949029.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d7b3f52cd8aa51387c6822",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-40%",
          "another_discount": [
            "FLASH SALE Gi\u1ea3m s\u1ed1c 40% t\u1ea5t c\u1ea3 c\u00e1c s\u1ea3n ph\u1ea9m n\u01b0\u1edbc"
          ]
        },
        "status": 1,
        "description_html": null,
        "slug": "chocolate-nong"
      }
    ]
  },
  {
    "style": 0,
    "id": 18,
    "name": "Th\u01b0\u1edfng th\u1ee9c t\u1ea1i nh\u00e0",
    "title": "Th\u01b0\u1edfng th\u1ee9c t\u1ea1i nh\u00e0",
    "thumbnail": "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/ca-phe-goi-ca-phe-uong-lien.png",
    "slug": "ca-phe-goi-ca-phe-uong-lien",
    "products": [
      {
        "id": "61e00bf0f7d6eb0019d93a51",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea H\u00f2a Tan \u0110\u1eadm V\u1ecb Vi\u1ec7t T\u00fai 40x16G",
        "description": "B\u1eaft \u0111\u1ea7u ng\u00e0y m\u1edbi v\u1edbi t\u00e1ch c\u00e0 ph\u00ea s\u1eefa \u201c\u0110\u1eadm v\u1ecb Vi\u1ec7t\u201d m\u1ea1nh m\u1ebd s\u1ebd gi\u00fap b\u1ea1n lu\u00f4n t\u1ec9nh t\u00e1o v\u00e0 h\u1ee9ng kh\u1edfi cho ng\u00e0y l\u00e0m vi\u1ec7c th\u1eadt hi\u1ec7u qu\u1ea3.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020197",
                "price": 0,
                "product_id": 2658,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2658
              }
            ]
          }
        ],
        "price": 99000,
        "base_price_str": "99.000\u0111",
        "price_str": "99.000\u0111",
        "base_price": 99000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1642353251_ca-phe-dam-vi-viet-tui-new_400x400.jpeg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1642353251_ca-phe-dam-vi-viet-tui-new.jpeg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-hoa-tan-dam-vi-viet-tui-40x16g"
      },
      {
        "id": "61e00bf0f7d6eb0019d93a52",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea Rang Xay Original 1 T\u00fai 1KG",
        "description": "C\u00e0 ph\u00ea Original 1 c\u1ee7a The Coffee House v\u1edbi 100% th\u00e0nh ph\u1ea7n c\u00e0 ph\u00ea Robusta \u0110\u0103k L\u0103k, v\u00f9ng tr\u1ed3ng c\u00e0 ph\u00ea ngon nh\u1ea5t Vi\u1ec7t Nam. B\u1eb1ng c\u00e1ch \u00e1p d\u1ee5ng k\u1ef9 thu\u1eadt rang xay hi\u1ec7n \u0111\u1ea1i, C\u00e0 ph\u00ea Original 1 mang \u0111\u1ebfn tr\u1ea3i nghi\u1ec7m tuy\u1ec7t v\u1eddi khi u\u1ed1ng c\u00e0 ph\u00ea t\u1ea1i nh\u00e0 v\u1edbi h\u01b0\u01a1ng v\u1ecb \u0111\u1eadm \u0111\u00e0 truy\u1ec1n th\u1ed1ng h\u1ee3p kh\u1ea9u v\u1ecb c\u1ee7a gi\u1edbi tr\u1ebb s\u00e0nh c\u00e0 ph\u00ea.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020196",
                "price": 0,
                "product_id": 2657,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2657
              }
            ]
          }
        ],
        "price": 235000,
        "base_price_str": "235.000\u0111",
        "price_str": "235.000\u0111",
        "base_price": 235000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1642387719_ori-1-1kg_400x400.jpeg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1642387719_ori-1-1kg.jpeg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-rang-xay-original-1-tui-1kg"
      },
      {
        "id": "61c4645e61af54001a4664d1",
        "recommended_id": null,
        "name": "Combo Qu\u00e0 T\u1ebft 2022",
        "description": "Combo qu\u00e0 T\u1ebft 2022: H\u1ed9p qu\u00e0 t\u1eb7ng v\u1edbi 4 h\u1ed9p tr\u00e0 t\u00fai l\u1ecdc Tearoma, H\u1ed9p c\u00e0 ph\u00ea s\u1eefa \u0111\u00e1, H\u1ed9p c\u00e0 ph\u00ea 3in1 \u0111\u1eadm v\u1ecb Vi\u1ec7t v\u00e0 C\u00e0 ph\u00ea Original 1 c\u1ee7a The Coffee House.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10110122",
                "price": 0,
                "product_id": 2653,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2653
              }
            ]
          }
        ],
        "price": 249000,
        "base_price_str": "321.000\u0111",
        "price_str": "249.000\u0111",
        "base_price": 321000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641441000_combo-du-vi-new_400x400.jpeg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641441000_combo-du-vi-new.jpeg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d5563985a1520ea86646d2",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-72.000\u0110",
          "another_discount": ["\u01afu \u0111\u00e3i Qu\u00e0 T\u1ebft 2022"]
        },
        "status": 1,
        "description_html": null,
        "slug": "combo-qua-tet-2022"
      },
      {
        "id": "61a4f0f461af54001a461a03",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea Ho\u00e0 Tan \u0110\u1eadm V\u1ecb Vi\u1ec7t (18 g\u00f3i x 16 gam)",
        "description": "B\u1eaft \u0111\u1ea7u ng\u00e0y m\u1edbi v\u1edbi t\u00e1ch c\u00e0 ph\u00ea s\u1eefa \u201c\u0110\u1eadm v\u1ecb Vi\u1ec7t\u201d m\u1ea1nh m\u1ebd s\u1ebd gi\u00fap b\u1ea1n lu\u00f4n t\u1ec9nh t\u00e1o v\u00e0 h\u1ee9ng kh\u1edfi cho ng\u00e0y l\u00e0m vi\u1ec7c th\u1eadt hi\u1ec7u qu\u1ea3.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020146",
                "price": 0,
                "product_id": 2631,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2631
              }
            ]
          }
        ],
        "price": 48000,
        "base_price_str": "48.000\u0111",
        "price_str": "48.000\u0111",
        "base_price": 48000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639648313_ca-phe-sua-da-hoa-tan-dam-vi-viet_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639648313_ca-phe-sua-da-hoa-tan-dam-vi-viet.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-hoa-tan-dam-vi-viet-18-goi-x-16-gam"
      },
      {
        "id": "61a4f0f461af54001a4619fc",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p C\u00e0 Ph\u00ea H\u00f2a Tan \u0110\u1eadm V\u1ecb Vi\u1ec7t",
        "description": "B\u1eaft \u0111\u1ea7u ng\u00e0y m\u1edbi v\u1edbi t\u00e1ch c\u00e0 ph\u00ea s\u1eefa \u201c\u0110\u1eadm v\u1ecb Vi\u1ec7t\u201d m\u1ea1nh m\u1ebd s\u1ebd gi\u00fap b\u1ea1n lu\u00f4n t\u1ec9nh t\u00e1o v\u00e0 h\u1ee9ng kh\u1edfi cho ng\u00e0y l\u00e0m vi\u1ec7c th\u1eadt hi\u1ec7u qu\u1ea3.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020147",
                "price": 0,
                "product_id": 2642,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2642
              }
            ]
          }
        ],
        "price": 109000,
        "base_price_str": "109.000\u0111",
        "price_str": "109.000\u0111",
        "base_price": 109000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641440252_1638341723-combo-ca-phe-3in1-damviviet_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641440252_1638341723-combo-ca-phe-3in1-damviviet.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-ca-phe-hoa-tan-dam-vi-viet"
      },
      {
        "id": "618659fe883e9c00128f7e75",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea Rang Xay Original 1",
        "description": "C\u00e0 ph\u00ea Original 1 c\u1ee7a The Coffee House v\u1edbi th\u00e0nh ph\u1ea7n ch\u00ednh c\u00e0 ph\u00ea Robusta \u0110\u1eafk L\u1eafk, v\u00f9ng tr\u1ed3ng c\u00e0 ph\u00ea n\u1ed5i ti\u1ebfng nh\u1ea5t Vi\u1ec7t Nam. B\u1eb1ng c\u00e1ch \u00e1p d\u1ee5ng k\u1ef9 thu\u1eadt rang xay hi\u1ec7n \u0111\u1ea1i, C\u00e0 ph\u00ea Original 1 mang \u0111\u1ebfn tr\u1ea3i nghi\u1ec7m tuy\u1ec7t v\u1eddi khi u\u1ed1ng c\u00e0 ph\u00ea t\u1ea1i nh\u00e0 v\u1edbi h\u01b0\u01a1ng v\u1ecb \u0111\u1eadm \u0111\u00e0 truy\u1ec1n th\u1ed1ng h\u1ee3p kh\u1ea9u v\u1ecb c\u1ee7a gi\u1edbi tr\u1ebb s\u00e0nh c\u00e0 ph\u00ea.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020131",
                "price": 0,
                "product_id": 2615,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2615
              }
            ]
          }
        ],
        "price": 60000,
        "base_price_str": "60.000\u0111",
        "price_str": "60.000\u0111",
        "base_price": 60000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639648297_ca-phe-rang-xay-original-1-250gr_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639648297_ca-phe-rang-xay-original-1-250gr.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1637231135_original1-lifestyle-3.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1637231135_original1-lifestyle-2.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1637231136_original1-lifestyle-1.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639648297_ca-phe-rang-xay-original-1-250gr.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-rang-xay-original-1"
      },
      {
        "id": "6196685bab06ba0019e2c39e",
        "recommended_id": null,
        "name": "Combo 2 C\u00e0 Ph\u00ea Rang Xay Original 1",
        "description": "C\u00e0 ph\u00ea Original 1 c\u1ee7a The Coffee House v\u1edbi th\u00e0nh ph\u1ea7n ch\u00ednh c\u00e0 ph\u00ea Robusta \u0110\u1eafk L\u1eafk, v\u00f9ng tr\u1ed3ng c\u00e0 ph\u00ea n\u1ed5i ti\u1ebfng nh\u1ea5t Vi\u1ec7t Nam. B\u1eb1ng c\u00e1ch \u00e1p d\u1ee5ng k\u1ef9 thu\u1eadt rang xay hi\u1ec7n \u0111\u1ea1i, C\u00e0 ph\u00ea Original 1 mang \u0111\u1ebfn tr\u1ea3i nghi\u1ec7m tuy\u1ec7t v\u1eddi khi u\u1ed1ng c\u00e0 ph\u00ea t\u1ea1i nh\u00e0 v\u1edbi h\u01b0\u01a1ng v\u1ecb \u0111\u1eadm \u0111\u00e0 truy\u1ec1n th\u1ed1ng h\u1ee3p kh\u1ea9u v\u1ecb c\u1ee7a gi\u1edbi tr\u1ebb s\u00e0nh c\u00e0 ph\u00ea.\n",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020142",
                "price": 0,
                "product_id": 2623,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2623
              }
            ]
          }
        ],
        "price": 99000,
        "base_price_str": "99.000\u0111",
        "price_str": "99.000\u0111",
        "base_price": 99000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1637288994_combo2-cforiginal-1_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1637288994_combo2-cforiginal-1.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-2-ca-phe-rang-xay-original-1"
      },
      {
        "id": "60c62c8215234d0011ede49e",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea S\u1eefa \u0110\u00e1 H\u00f2a Tan",
        "description": "Th\u1eadt d\u1ec5 d\u00e0ng \u0111\u1ec3 b\u1eaft \u0111\u1ea7u ng\u00e0y m\u1edbi v\u1edbi t\u00e1ch c\u00e0 ph\u00ea s\u1eefa \u0111\u00e1 s\u00f3ng s\u00e1nh, th\u01a1m ngon nh\u01b0 c\u00e0 ph\u00ea pha phin.\nV\u1ecb \u0111\u1eafng thanh c\u1ee7a c\u00e0 ph\u00ea ho\u00e0 quy\u1ec7n v\u1edbi v\u1ecb ng\u1ecdt b\u00e9o c\u1ee7a s\u1eefa, gi\u00fap b\u1ea1n lu\u00f4n t\u1ec9nh t\u00e1o v\u00e0 h\u1ee9ng kh\u1edfi cho ng\u00e0y l\u00e0m vi\u1ec7c th\u1eadt hi\u1ec7u qu\u1ea3.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020106",
                "price": 0,
                "product_id": 2567,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2567
              }
            ]
          }
        ],
        "price": 44000,
        "base_price_str": "44.000\u0111",
        "price_str": "44.000\u0111",
        "base_price": 44000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/cpsd-3in1_971575_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/cpsd-3in1_971575.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-sua-da-hoa-tan"
      },
      {
        "id": "61224f81ef16be001293cccd",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p C\u00e0 Ph\u00ea S\u1eefa \u0110\u00e1 H\u00f2a Tan",
        "description": "Th\u1eadt d\u1ec5 d\u00e0ng \u0111\u1ec3 b\u1eaft \u0111\u1ea7u ng\u00e0y m\u1edbi v\u1edbi t\u00e1ch c\u00e0 ph\u00ea s\u1eefa \u0111\u00e1 s\u00f3ng s\u00e1nh, th\u01a1m ngon nh\u01b0 c\u00e0 ph\u00ea pha phin. V\u1ecb \u0111\u1eafng thanh c\u1ee7a c\u00e0 ph\u00ea ho\u00e0 quy\u1ec7n v\u1edbi v\u1ecb ng\u1ecdt b\u00e9o c\u1ee7a s\u1eefa, gi\u00fap b\u1ea1n lu\u00f4n t\u1ec9nh t\u00e1o v\u00e0 h\u1ee9ng kh\u1edfi cho ng\u00e0y l\u00e0m vi\u1ec7c th\u1eadt hi\u1ec7u qu\u1ea3.\n",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020119",
                "price": 0,
                "product_id": 2581,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2581
              }
            ]
          }
        ],
        "price": 109000,
        "base_price_str": "109.000\u0111",
        "price_str": "109.000\u0111",
        "base_price": 109000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/combo-3cfsd-nopromo_320619_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/combo-3cfsd-nopromo_320619.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-ca-phe-sua-da-hoa-tan"
      },
      {
        "id": "618659fe883e9c00128f7e74",
        "recommended_id": null,
        "name": " C\u00e0 Ph\u00ea S\u1eefa \u0110\u00e1 H\u00f2a Tan T\u00fai 25x22G",
        "description": "Th\u1eadt d\u1ec5 d\u00e0ng \u0111\u1ec3 b\u1eaft \u0111\u1ea7u ng\u00e0y m\u1edbi v\u1edbi t\u00e1ch c\u00e0 ph\u00ea s\u1eefa \u0111\u00e1 s\u00f3ng s\u00e1nh, th\u01a1m ngon nh\u01b0 c\u00e0 ph\u00ea pha phin. V\u1ecb \u0111\u1eafng thanh c\u1ee7a c\u00e0 ph\u00ea ho\u00e0 quy\u1ec7n v\u1edbi v\u1ecb ng\u1ecdt b\u00e9o c\u1ee7a s\u1eefa, gi\u00fap b\u1ea1n lu\u00f4n t\u1ec9nh t\u00e1o v\u00e0 h\u1ee9ng kh\u1edfi cho ng\u00e0y l\u00e0m vi\u1ec7c th\u1eadt hi\u1ec7u qu\u1ea3.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020132",
                "price": 0,
                "product_id": 2616,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2616
              }
            ]
          }
        ],
        "price": 99000,
        "base_price_str": "99.000\u0111",
        "price_str": "99.000\u0111",
        "base_price": 99000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639648355_ca-phe-sua-da-hoa-tan-tui-25x22gr_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639648355_ca-phe-sua-da-hoa-tan-tui-25x22gr.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-sua-da-hoa-tan-tui-25x22g"
      },
      {
        "id": "60f5531c861e550018fd1fca",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea S\u1eefa \u0110\u00e1 Pack 6 Lon",
        "description": "V\u1edbi thi\u1ebft k\u1ebf lon cao tr\u1ebb trung, hi\u1ec7n \u0111\u1ea1i v\u00e0 ti\u1ec7n l\u1ee3i, C\u00e0 ph\u00ea s\u1eefa \u0111\u00e1 lon th\u01a1m ngon \u0111\u1eadm v\u1ecb c\u1ee7a The Coffee House s\u1ebd \u0111\u1ed3ng h\u00e0nh c\u00f9ng nh\u1ecbp s\u1ed1ng s\u00f4i n\u1ed5i c\u1ee7a tu\u1ed5i tr\u1ebb v\u00e0 gi\u00fap b\u1ea1n c\u00f3 \u0111\u01b0\u1ee3c m\u1ed9t ng\u00e0y l\u00e0m vi\u1ec7c \u0111\u1ea7y h\u1ee9ng kh\u1edfi.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020112",
                "price": 0,
                "product_id": 2575,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2575
              }
            ]
          }
        ],
        "price": 84000,
        "base_price_str": "84.000\u0111",
        "price_str": "84.000\u0111",
        "base_price": 84000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/p6-lon-3in1_717216_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/p6-lon-3in1_717216.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-sua-da-pack-6-lon"
      },
      {
        "id": "60dbe43f5775f50018c71ea8",
        "recommended_id": null,
        "name": "Th\u00f9ng 24 Lon C\u00e0 Ph\u00ea S\u1eefa \u0110\u00e1 ",
        "description": "V\u1edbi thi\u1ebft k\u1ebf lon cao tr\u1ebb trung, hi\u1ec7n \u0111\u1ea1i v\u00e0 ti\u1ec7n l\u1ee3i, C\u00e0 ph\u00ea s\u1eefa \u0111\u00e1 lon th\u01a1m ngon \u0111\u1eadm v\u1ecb c\u1ee7a The Coffee House s\u1ebd \u0111\u1ed3ng h\u00e0nh c\u00f9ng nh\u1ecbp s\u1ed1ng s\u00f4i n\u1ed5i c\u1ee7a tu\u1ed5i tr\u1ebb v\u00e0 gi\u00fap b\u1ea1n c\u00f3 \u0111\u01b0\u1ee3c m\u1ed9t ng\u00e0y l\u00e0m vi\u1ec7c \u0111\u1ea7y h\u1ee9ng kh\u1edfi.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020109",
                "price": 0,
                "product_id": 2572,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2572
              }
            ]
          }
        ],
        "price": 269000,
        "base_price_str": "336.000\u0111",
        "price_str": "269.000\u0111",
        "base_price": 336000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/24-lon-cpsd_225680_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/24-lon-cpsd_225680.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d5563985a1520ea86646d2",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-67.000\u0110",
          "another_discount": ["\u01afu \u0111\u00e3i Qu\u00e0 T\u1ebft 2022"]
        },
        "status": 1,
        "description_html": null,
        "slug": "thung-24-lon-ca-phe-sua-da"
      },
      {
        "id": "60c62c8215234d0011ede4a0",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea Rich Finish Gu \u0110\u1eadm Tinh T\u1ebf (350G)",
        "description": "The Coffee House  hi\u1ec3u r\u1eb1ng ly c\u00e0 ph\u00ea ngon ph\u1ea3i \u0111\u1eadm \u0111\u00e0, r\u00f5 v\u1ecb t\u1eeb c\u00e1i ch\u1ea1m \u0111\u1ea7u ti\u00ean \u0111\u1ebfn h\u1eadu v\u1ecb v\u01b0\u01a1ng v\u1ea5n. C\u00e0 ph\u00ea Rich Finish mang \u0111\u1ebfn nh\u1eefng ly c\u00e0 ph\u00ea \u0111\u1eadm, th\u01a1m, h\u01b0\u01a1ng v\u1ecb tinh t\u1ebf gi\u00fap b\u1ea1n b\u1eaft \u0111\u1ea7u ng\u00e0y m\u1edbi \u0111\u1ea7y n\u0103ng l\u01b0\u1ee3ng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020103",
                "price": 0,
                "product_id": 2565,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2565
              }
            ]
          }
        ],
        "price": 90000,
        "base_price_str": "90.000\u0111",
        "price_str": "90.000\u0111",
        "base_price": 90000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/rich-finish-nopromo_485968_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/rich-finish-nopromo_485968.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-rich-finish-gu-dam-tinh-te-350g"
      },
      {
        "id": "60c62c8215234d0011ede49f",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea Peak Flavor H\u01b0\u01a1ng Th\u01a1m \u0110\u1ec9nh Cao (350G)",
        "description": "\u0110\u01b0\u1ee3c rang d\u01b0\u1edbi nhi\u1ec7t \u0111\u1ed9 v\u00e0ng, C\u00e0 ph\u00ea Peak Flavor - H\u01b0\u01a1ng th\u01a1m \u0111\u1ec9nh cao l\u01b0u gi\u1eef tr\u1ecdn v\u1eb9n h\u01b0\u01a1ng th\u01a1m tinh t\u1ebf \u0111\u1eb7c tr\u01b0ng c\u1ee7a c\u00e0 ph\u00ea Robusta \u0110\u0103k N\u00f4ng v\u00e0 Arabica C\u1ea7u \u0110\u1ea5t. V\u1edbi s\u1ef1 h\u00f2a tr\u1ed9n nhi\u1ec1u cung b\u1eadc gi\u1eefa h\u01b0\u01a1ng v\u00e0 v\u1ecb s\u1ebd mang \u0111\u1ebfn cho b\u1ea1n m\u1ed9t ng\u00e0y m\u1edbi tr\u00e0n \u0111\u1ea7y c\u1ea3m h\u1ee9ng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020104",
                "price": 0,
                "product_id": 2566,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2566
              }
            ]
          }
        ],
        "price": 90000,
        "base_price_str": "90.000\u0111",
        "price_str": "90.000\u0111",
        "base_price": 90000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/peak-plavor-nopromo_715372_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/peak-plavor-nopromo_715372.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-peak-flavor-huong-thom-dinh-cao-350g"
      },
      {
        "id": "5d92fbc89e12b47db8cabbd7",
        "recommended_id": null,
        "name": "C\u00e0 Ph\u00ea Nguy\u00ean H\u1ea1t Arabica TCH (200gr)",
        "description": "The Coffee House g\u1eedi b\u1ea1n h\u01b0\u01a1ng v\u1ecb c\u00e0 ph\u00ea t\u1eeb 100% h\u1ea1t Arabica, \u0111\u01b0\u1ee3c ch\u1ecdn l\u1ecdc k\u1ef9 l\u01b0\u1ee1ng t\u1ea1i v\u00f9ng C\u1ea7u \u0110\u1ea5t, v\u1edbi \u0111\u1ed9 cao 1650m. V\u1edbi v\u1ecb \u0111\u1eafng nh\u1eb9, h\u1eadu v\u1ecb chua thanh, ng\u1ecdt d\u1ecbu, C\u00e0 ph\u00ea nguy\u00ean h\u1ea1t Arabica t\u1eeb The Coffee House s\u1ebd gi\u00fap b\u1ea1n t\u1ea1o ra nh\u1eefng t\u00e1ch c\u00e0 ph\u00ea cold brew hay hand brew tinh t\u1ebf y\u00eau th\u00edch c\u1ee7a ri\u00eang m\u00ecnh.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "10070008",
                "price": 0,
                "product_id": 2090,
                "val": "Kh\u00e1ch h\u00e0ng t\u1ef1 xay h\u1ea1t t\u1ea1i nh\u00e0",
                "localize": {
                  "en": "Kh\u00e1ch h\u00e0ng t\u1ef1 xay h\u1ea1t t\u1ea1i nh\u00e0",
                  "vi": "Kh\u00e1ch h\u00e0ng t\u1ef1 xay h\u1ea1t t\u1ea1i nh\u00e0"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Kh\u00e1ch h\u00e0ng t\u1ef1 xay h\u1ea1t t\u1ea1i nh\u00e0",
                "id": 2090
              },
              {
                "code": "11020075",
                "price": 0,
                "product_id": 2206,
                "val": "C\u1eeda h\u00e0ng xay s\u1eb5n h\u1ea1t th\u00e0nh d\u1ea1ng b\u1ed9t",
                "localize": {
                  "vi": "C\u1eeda h\u00e0ng xay s\u1eb5n h\u1ea1t th\u00e0nh d\u1ea1ng b\u1ed9t",
                  "en": "C\u1eeda h\u00e0ng xay s\u1eb5n h\u1ea1t th\u00e0nh d\u1ea1ng b\u1ed9t"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "C\u1eeda h\u00e0ng xay s\u1eb5n h\u1ea1t th\u00e0nh d\u1ea1ng b\u1ed9t",
                "id": 2206
              }
            ]
          }
        ],
        "price": 100000,
        "base_price_str": "100.000\u0111",
        "price_str": "100.000\u0111",
        "base_price": 100000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/Arabica_1_109041_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/Arabica_1_109041.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "ca-phe-nguyen-hat-arabica-tch-200gr"
      },
      {
        "id": "618659fe883e9c00128f7e76",
        "recommended_id": null,
        "name": "Tr\u00e0 Xanh L\u00e1 Tearoma 100G",
        "description": "Tr\u00e0 Xanh Tearoma kh\u00f4ng c\u00f2n qu\u00e1 xa l\u1ea1 v\u1edbi ng\u01b0\u1eddi Vi\u1ec7t, h\u01b0\u01a1ng tr\u00e0 xanh \u0111\u1eb7c tr\u01b0ng thoang tho\u1ea3ng t\u1eeb b\u00fap tr\u00e0 t\u01b0\u01a1i ph\u1ee7 s\u01b0\u01a1ng t\u1ef1 nhi\u00ean, c\u00f9ng h\u01b0\u01a1ng v\u1ecb \u0111\u1eadm \u0111\u00e0 \u0111\u1eb7c tr\u01b0ng kh\u00f3 qu\u00ean. ",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020141",
                "price": 0,
                "product_id": 2614,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2614
              }
            ]
          }
        ],
        "price": 75000,
        "base_price_str": "75.000\u0111",
        "price_str": "75.000\u0111",
        "base_price": 75000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639648162_tra-xanh-la-tearoma-100gr_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639648162_tra-xanh-la-tearoma-100gr.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639648162_tearoma-loosetea-tra-xanh1.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "tra-xanh-la-tearoma-100g"
      },
      {
        "id": "618659fe883e9c00128f7e7a",
        "recommended_id": null,
        "name": "Tr\u00e0 Oolong L\u00e1 Tearoma 100G",
        "description": "Tr\u00e0 Oolong Tearoma  \u0111\u01b0\u1ee3c ch\u1ecdn l\u1ecdc  b\u1edfi c\u00e1c b\u00fap tr\u00e0 non ph\u1ee7 s\u01b0\u01a1ng, t\u01b0\u01a1i m\u00e1t tr\u00ean cao nguy\u00ean L\u00e2m \u0110\u1ed3ng. C\u00f3 m\u00f9i h\u01b0\u01a1ng d\u1ecbu nh\u1eb9 ho\u00e0n to\u00e0n t\u1eeb t\u1ef1 nhi\u00ean, v\u1ecb h\u1eadu ng\u1ecdt.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020137",
                "price": 0,
                "product_id": 2610,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2610
              }
            ]
          }
        ],
        "price": 100000,
        "base_price_str": "100.000\u0111",
        "price_str": "100.000\u0111",
        "base_price": 100000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639648109_tra-oolong-la-tearoma-100gr_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639648109_tra-oolong-la-tearoma-100gr.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639648109_tearoma-loosetea-oolong.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "tra-oolong-la-tearoma-100g"
      },
      {
        "id": "618659fe883e9c00128f7e79",
        "recommended_id": null,
        "name": "Tr\u00e0 Oolong T\u00fai L\u1ecdc Tearoma 20x2G",
        "description": "Tr\u00e0 Oolong t\u00fai l\u1ecdc v\u1edbi m\u00f9i h\u01b0\u01a1ng d\u1ecbu nh\u1eb9 ho\u00e0n to\u00e0n t\u1eeb t\u1ef1 nhi\u00ean, v\u1ecb h\u1eadu ng\u1ecdt,  v\u00e0 h\u01b0\u01a1ng th\u01a1m tinh t\u1ebf. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0,... nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0. ",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020138",
                "price": 0,
                "product_id": 2611,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2611
              }
            ]
          }
        ],
        "price": 28000,
        "base_price_str": "28.000\u0111",
        "price_str": "28.000\u0111",
        "base_price": 28000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639646969_tra-oolong-tui-loc-tearoma-20x2gr_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639646968_tra-oolong-tui-loc-tearoma-20x2gr.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639646986_tra-oolong-tui-loc-tearoma-20x2gr.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639647002_copy-of-tearoma-teabag-3-1.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "tra-oolong-tui-loc-tearoma-20x2g"
      },
      {
        "id": "61a4f0f361af54001a4619f8",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p Tr\u00e0 Oolong T\u00fai L\u1ecdc Tearoma",
        "description": "Tr\u00e0 Oolong t\u00fai l\u1ecdc v\u1edbi m\u00f9i h\u01b0\u01a1ng d\u1ecbu nh\u1eb9 ho\u00e0n to\u00e0n t\u1eeb t\u1ef1 nhi\u00ean, v\u1ecb h\u1eadu ng\u1ecdt, v\u00e0 h\u01b0\u01a1ng th\u01a1m tinh t\u1ebf. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0,... nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020151",
                "price": 0,
                "product_id": 2646,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2646
              }
            ]
          }
        ],
        "price": 69000,
        "base_price_str": "69.000\u0111",
        "price_str": "69.000\u0111",
        "base_price": 69000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641440292_1638341615-combo-tra-oolong-400x400_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641440292_1638341615-combo-tra-oolong-400x400.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-tra-oolong-tui-loc-tearoma"
      },
      {
        "id": "618659fe883e9c00128f7e7c",
        "recommended_id": null,
        "name": "Tr\u00e0 L\u00e0i L\u00e1 Tearoma 100G",
        "description": "Tr\u00e0 Tearoma h\u01b0\u01a1ng l\u00e0i tinh t\u1ebf, thanh m\u00e1t, tr\u00ean n\u1ec1n tr\u00e0 xanh \u0111\u1eadm \u0111\u00e0 kh\u00f3 qu\u00ean.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020135",
                "price": 0,
                "product_id": 2608,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2608
              }
            ]
          }
        ],
        "price": 80000,
        "base_price_str": "80.000\u0111",
        "price_str": "80.000\u0111",
        "base_price": 80000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639648192_tra-lai-la-tearoma-100gr_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639648191_tra-lai-la-tearoma-100gr.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "tra-lai-la-tearoma-100g"
      },
      {
        "id": "618659fe883e9c00128f7e7b",
        "recommended_id": null,
        "name": " Tr\u00e0 L\u00e0i T\u00fai L\u1ecdc Tearoma 20x2G",
        "description": "Tr\u00e0 t\u00fai l\u1ecdc Tearoma h\u01b0\u01a1ng l\u00e0i th\u01a1m tinh t\u1ebf, thanh m\u00e1t, tr\u00ean n\u1ec1n tr\u00e0 xanh \u0111\u1eadm \u0111\u00e0 kh\u00f3 qu\u00ean. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0,.. nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0. ",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020136",
                "price": 0,
                "product_id": 2609,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2609
              }
            ]
          }
        ],
        "price": 28000,
        "base_price_str": "28.000\u0111",
        "price_str": "28.000\u0111",
        "base_price": 28000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639647075_tra-lai-tui-loc-tearoma-20x2gr_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639647075_tra-lai-tui-loc-tearoma-20x2gr.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639647064_tra-lai-tui-loc-tearoma-20x2gr.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639647065_tearoma-teabag4.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "tra-lai-tui-loc-tearoma-20x2g"
      },
      {
        "id": "61a4f0f461af54001a4619f9",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p Tr\u00e0 L\u00e0i T\u00fai L\u1ecdc Tearoma",
        "description": "Tr\u00e0 t\u00fai l\u1ecdc Tearoma h\u01b0\u01a1ng l\u00e0i th\u01a1m tinh t\u1ebf, thanh m\u00e1t, tr\u00ean n\u1ec1n tr\u00e0 xanh \u0111\u1eadm \u0111\u00e0 kh\u00f3 qu\u00ean. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0,.. nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020150",
                "price": 0,
                "product_id": 2645,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2645
              }
            ]
          }
        ],
        "price": 69000,
        "base_price_str": "69.000\u0111",
        "price_str": "69.000\u0111",
        "base_price": 69000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641440398_1638341675-combo-tra-huong-lai_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641440398_1638341675-combo-tra-huong-lai.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-tra-lai-tui-loc-tearoma"
      },
      {
        "id": "618659fe883e9c00128f7e78",
        "recommended_id": null,
        "name": "Tr\u00e0 Sen L\u00e1 Tearoma 100G",
        "description": "Tr\u00e0 Tearoma h\u01b0\u01a1ng sen tinh t\u1ebf, thanh m\u00e1t, tr\u00ean n\u1ec1n tr\u00e0 xanh \u0111\u1eadm \u0111\u00e0, \u0111\u1eb7c tr\u01b0ng kh\u00f3 qu\u00ean.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020139",
                "price": 0,
                "product_id": 2612,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2612
              }
            ]
          }
        ],
        "price": 80000,
        "base_price_str": "80.000\u0111",
        "price_str": "80.000\u0111",
        "base_price": 80000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639648222_tra-sen-la-tearoma-100gr_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639648222_tra-sen-la-tearoma-100gr.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639648221_tearoma-loosetea-huongsen1.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "tra-sen-la-tearoma-100g"
      },
      {
        "id": "618659fe883e9c00128f7e77",
        "recommended_id": null,
        "name": "Tr\u00e0 Sen T\u00fai L\u1ecdc Tearoma 20x2G",
        "description": "Tr\u00e0 t\u00fai l\u1ecdc Tearoma h\u01b0\u01a1ng sen tinh t\u1ebf, thanh m\u00e1t, tr\u00ean n\u1ec1n tr\u00e0 xanh \u0111\u1eadm \u0111\u00e0 kh\u00f3 qu\u00ean. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0, \u0111i du l\u1ecbch,... nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020140",
                "price": 0,
                "product_id": 2613,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2613
              }
            ]
          }
        ],
        "price": 28000,
        "base_price_str": "28.000\u0111",
        "price_str": "28.000\u0111",
        "base_price": 28000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639648068_tra-sen-tui-loc-tearoma-20x2gr_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639648068_tra-sen-tui-loc-tearoma-20x2gr.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639648068_tra-sen-tui-loc-tearoma-20x2gr.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639648068_tearoma-teabag4.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "tra-sen-tui-loc-tearoma-20x2g"
      },
      {
        "id": "61a4f0f461af54001a4619fa",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p Tr\u00e0 Sen T\u00fai L\u1ecdc Tearoma",
        "description": "Tr\u00e0 t\u00fai l\u1ecdc Tearoma h\u01b0\u01a1ng sen tinh t\u1ebf, thanh m\u00e1t, tr\u00ean n\u1ec1n tr\u00e0 xanh \u0111\u1eadm \u0111\u00e0 kh\u00f3 qu\u00ean. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0, \u0111i du l\u1ecbch,... nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020149",
                "price": 0,
                "product_id": 2644,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2644
              }
            ]
          }
        ],
        "price": 69000,
        "base_price_str": "69.000\u0111",
        "price_str": "69.000\u0111",
        "base_price": 69000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641440433_1638341689-combo-tra-huong-sen-400x400_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641440433_1638341689-combo-tra-huong-sen-400x400.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-tra-sen-tui-loc-tearoma"
      },
      {
        "id": "618659fe883e9c00128f7e7d",
        "recommended_id": null,
        "name": "Tr\u00e0 \u0110\u00e0o T\u00fai L\u1ecdc Tearoma 20x2G",
        "description": "Tr\u00e0 t\u00fai l\u1ecdc Tearoma h\u01b0\u01a1ng \u0111\u00e0o v\u1edbi h\u01b0\u01a1ng th\u01a1m tinh t\u1ebf v\u00e0 ho\u00e0n to\u00e0n t\u1ef1 nhi\u00ean, c\u00f9ng n\u1ec1n tr\u00e0 \u0111en \u0111\u1eadm v\u1ecb kh\u00f3 qu\u00ean. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0,.. nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0. ",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020134",
                "price": 0,
                "product_id": 2607,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2607
              }
            ]
          }
        ],
        "price": 28000,
        "base_price_str": "28.000\u0111",
        "price_str": "28.000\u0111",
        "base_price": 28000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1639646846_tra-dao-tui-loc-tearoma-20x2gr_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1639646846_tra-dao-tui-loc-tearoma-20x2gr.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639646845_tearoma-teabag-2-1.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639646846_copy-of-tearoma-teabag-3-1.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639646846_tearoma-teabag1-1.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "tra-dao-tui-loc-tearoma-20x2g"
      },
      {
        "id": "61a4f0f461af54001a4619fb",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p Tr\u00e0 \u0110\u00e0o T\u00fai L\u1ecdc Tearoma",
        "description": "Tr\u00e0 t\u00fai l\u1ecdc Tearoma h\u01b0\u01a1ng \u0111\u00e0o v\u1edbi h\u01b0\u01a1ng th\u01a1m tinh t\u1ebf v\u00e0 ho\u00e0n to\u00e0n t\u1ef1 nhi\u00ean, c\u00f9ng n\u1ec1n tr\u00e0 \u0111en \u0111\u1eadm v\u1ecb kh\u00f3 qu\u00ean. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0,.. nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020148",
                "price": 0,
                "product_id": 2643,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2643
              }
            ]
          }
        ],
        "price": 69000,
        "base_price_str": "69.000\u0111",
        "price_str": "69.000\u0111",
        "base_price": 69000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641440460_1638344058-combo-tra-huong-dao-400x400_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641440460_1638344058-combo-tra-huong-dao-400x400.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-tra-dao-tui-loc-tearoma"
      },
      {
        "id": "61a4f0f361af54001a4619f7",
        "recommended_id": null,
        "name": "Giftset Tr\u00e0 Tearoma",
        "description": "H\u1ed9p qu\u00e0 t\u1eb7ng v\u1edbi 4 h\u1ed9p tr\u00e0 t\u00fai l\u1ecdc Tearoma c\u00e1c lo\u1ea1i l\u00e0 m\u00f3n qu\u00e0 th\u1eadt \u00fd ngh\u0129a cho nh\u1eefng ng\u01b0\u1eddi th\u00e2n y\u00eau trong d\u1ecbp n\u00e0y.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020153",
                "price": 0,
                "product_id": 2647,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2647
              }
            ]
          }
        ],
        "price": 139000,
        "base_price_str": "169.000\u0111",
        "price_str": "139.000\u0111",
        "base_price": 169000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641440575_gift-set-tearoma-1_400x400.jpeg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641440575_gift-set-tearoma-1.jpeg",
          "https://minio.thecoffeehouse.com/image/admin/1639645311_gift-set-tearoma-2-1.jpg",
          "https://minio.thecoffeehouse.com/image/admin/1639645312_gift-set-tearoma-3-1.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": {
          "id": "61d5563985a1520ea86646d2",
          "discount_start": 1643216400,
          "discount_end": 1643299200,
          "title": null,
          "discount_percent": "-30.000\u0110",
          "another_discount": ["\u01afu \u0111\u00e3i Qu\u00e0 T\u1ebft 2022"]
        },
        "status": 1,
        "description_html": null,
        "slug": "giftset-tra-tearoma"
      }
    ]
  },
  {
    "style": 0,
    "id": 12,
    "name": "B\u00e1nh - Snack",
    "title": "B\u00e1nh - Snack",
    "thumbnail": "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/banh-snack.png",
    "slug": "banh-man-banh-ngot-snack",
    "products": [
      {
        "id": "5cdb9699696fb3793d4025ab",
        "recommended_id": null,
        "name": "B\u00e1nh M\u00ec Que Pate",
        "description": "V\u1ecf b\u00e1nh m\u00ec gi\u00f2n tan, k\u1ebft h\u1ee3p v\u1edbi l\u1edbp nh\u00e2n pate b\u00e9o b\u00e9o \u0111\u1eadm \u0111\u00e0 s\u1ebd l\u00e0 l\u1ef1a ch\u1ecdn l\u00fd t\u01b0\u1edfng nh\u1eb9 nh\u00e0ng \u0111\u1ec3 l\u1ea5p \u0111\u1ea7y chi\u1ebfc b\u1ee5ng \u0111\u00f3i , cho 1 b\u1eefa s\u00e1ng - tr\u01b0a - chi\u1ec1u - t\u1ed1i c\u1ee7a b\u1ea1n th\u00eam ph\u1ea7n th\u00fa v\u1ecb.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040176",
                "price": 0,
                "product_id": 1931,
                "val": "V\u1eeba",
                "localize": {
                  "en": null,
                  "vi": "Size V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 1931
              }
            ]
          }
        ],
        "price": 12000,
        "base_price_str": "12.000\u0111",
        "price_str": "12.000\u0111",
        "base_price": 12000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/banhmique_056830_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/banhmique_056830.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": ["l\u00e0m n\u00f3ng b\u00e1nh"],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "banh-mi-que-pate"
      },
      {
        "id": "5cdb9624696fb37838048d69",
        "recommended_id": null,
        "name": "B\u00e1nh M\u00ec Que Pate Cay",
        "description": "V\u1ecf b\u00e1nh m\u00ec gi\u00f2n tan, k\u1ebft h\u1ee3p v\u1edbi l\u1edbp nh\u00e2n pate b\u00e9o b\u00e9o \u0111\u1eadm \u0111\u00e0 v\u00e0 1 ch\u00fat cay cay s\u1ebd l\u00e0 l\u1ef1a ch\u1ecdn l\u00fd t\u01b0\u1edfng nh\u1eb9 nh\u00e0ng \u0111\u1ec3 l\u1ea5p \u0111\u1ea7y chi\u1ebfc b\u1ee5ng \u0111\u00f3i , cho 1 b\u1eefa s\u00e1ng - tr\u01b0a - chi\u1ec1u - t\u1ed1i c\u1ee7a b\u1ea1n th\u00eam ph\u1ea7n th\u00fa v\u1ecb.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040192",
                "price": 0,
                "product_id": 1949,
                "val": "V\u1eeba",
                "localize": {
                  "en": null,
                  "vi": "Size V\u1eeba"
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 1949
              }
            ]
          }
        ],
        "price": 12000,
        "base_price_str": "12.000\u0111",
        "price_str": "12.000\u0111",
        "base_price": 12000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/banhmique_683851_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/banhmique_683851.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": ["l\u00e0m n\u00f3ng b\u00e1nh"],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "banh-mi-que-pate-cay"
      },
      {
        "id": "5ee30511e852ce0011e71b04",
        "recommended_id": null,
        "name": "B\u00e1nh M\u00ec VN Th\u1ecbt Ngu\u1ed9i",
        "description": "G\u00f3i g\u1ecdn trong \u1ed5 b\u00e1nh m\u00ec Vi\u1ec7t Nam, l\u00e0 t\u1eebng l\u1edbp ch\u1ea3, t\u1eebng l\u1edbp jambon h\u00f2a quy\u1ec7n c\u00f9ng b\u01a1 v\u00e0 pate th\u01a1m l\u1eebng, th\u00eam d\u01b0a rau cho b\u1eefa s\u00e1ng \u0111\u1ea7y n\u0103ng l\u01b0\u1ee3ng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040219",
                "price": 0,
                "product_id": 2242,
                "localize": {
                  "en": "Kh\u00f4ng Cay",
                  "vi": "Kh\u00f4ng Cay"
                },
                "val": "Kh\u00f4ng Cay",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "Kh\u00f4ng Cay",
                "id": 2242
              }
            ]
          }
        ],
        "price": 29000,
        "base_price_str": "29.000\u0111",
        "price_str": "29.000\u0111",
        "base_price": 29000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1638440015_banh-mi-vietnam_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1638440015_banh-mi-vietnam.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [
          "l\u00e0m n\u00f3ng b\u00e1nh",
          "kh\u00f4ng t\u01b0\u01a1ng \u1edbt"
        ],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "banh-mi-vn-thit-nguoi"
      },
      {
        "id": "5b446b501acd4d11c371b00c",
        "recommended_id": null,
        "name": "Croissant tr\u1ee9ng mu\u1ed1i",
        "description": "Croissant tr\u1ee9ng mu\u1ed1i th\u01a1m l\u1eebng, b\u00ean ngo\u00e0i v\u1ecf b\u00e1nh gi\u00f2n h\u1ea5p d\u1eabn b\u00ean trong tr\u1ee9ng mu\u1ed1i v\u1ecb ngon kh\u00f3 c\u01b0\u1ee1ng.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": "1368",
                "code": "11040146",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "Size v\u1eeba",
                  "en": null
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 1368
              }
            ]
          }
        ],
        "price": 35000,
        "base_price_str": "35.000\u0111",
        "price_str": "35.000\u0111",
        "base_price": 35000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/croissant-trung-muoi_880850_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/croissant-trung-muoi_880850.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "croissant-trung-muoi"
      },
      {
        "id": "5d92fbc79e12b47db8cabbcf",
        "recommended_id": null,
        "name": "Ch\u00e0 B\u00f4ng Ph\u00f4 Mai",
        "description": "Chi\u1ebfc b\u00e1nh v\u1edbi l\u1edbp ph\u00f4 mai v\u00e0ng s\u00e1nh m\u1ecbn b\u00ean trong, \u0111\u01b0\u1ee3c b\u1ecdc ngo\u00e0i l\u1edbp v\u1ecf x\u1ed1p m\u1ec1m th\u01a1m l\u1eebng. Th\u00eam l\u1edbp ch\u00e0 b\u00f4ng m\u1eb1n m\u1eb7n h\u1ea5p d\u1eabn b\u00ean tr\u00ean.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040162",
                "price": 0,
                "product_id": 2101,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2101
              }
            ]
          }
        ],
        "price": 32000,
        "base_price_str": "32.000\u0111",
        "price_str": "32.000\u0111",
        "base_price": 32000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/cha-bong-pho-mai_204282_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/cha-bong-pho-mai_204282.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "cha-bong-pho-mai"
      },
      {
        "id": "6051cdb617a9cf0011ee6d8f",
        "recommended_id": null,
        "name": "Mochi Kem Ph\u00fac B\u1ed3n T\u1eed",
        "description": "Bao b\u1ecdc b\u1edfi l\u1edbp v\u1ecf Mochi d\u1ebbo th\u01a1m, b\u00ean trong l\u00e0 l\u1edbp kem l\u1ea1nh c\u00f9ng nh\u00e2n ph\u00fac b\u1ed3n t\u1eed ng\u1ecdt ng\u00e0o. G\u1ecdi 1 chi\u1ebfc Mochi cho ng\u00e0y th\u1eadt t\u01b0\u01a1i m\u00e1t.\nS\u1ea3n ph\u1ea9m ph\u1ea3i b\u1ea3o qu\u00e1n m\u00e1t v\u00e0 d\u00f9ng ngon nh\u1ea5t trong 2h sau khi nh\u1eadn h\u00e0ng.\n\n",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040273",
                "price": 0,
                "product_id": 2526,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2526
              }
            ]
          }
        ],
        "price": 19000,
        "base_price_str": "19.000\u0111",
        "price_str": "19.000\u0111",
        "base_price": 19000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1643102019_mochi-phucbontu_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1643102019_mochi-phucbontu.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "mochi-kem-phuc-bon-tu"
      },
      {
        "id": "6051cdb617a9cf0011ee6d90",
        "recommended_id": null,
        "name": "Mochi Kem Vi\u1ec7t Qu\u1ea5t",
        "description": "Bao b\u1ecdc b\u1edfi l\u1edbp v\u1ecf Mochi d\u1ebbo th\u01a1m, b\u00ean trong l\u00e0 l\u1edbp kem l\u1ea1nh c\u00f9ng nh\u00e2n vi\u1ec7t qu\u1ea5t \u0111\u1eb7c tr\u01b0ng th\u01a1m th\u01a1m, ng\u1ecdt d\u1ecbu. G\u1ecdi 1 chi\u1ebfc Mochi cho ng\u00e0y th\u1eadt t\u01b0\u01a1i m\u00e1t. \nS\u1ea3n ph\u1ea9m ph\u1ea3i b\u1ea3o qu\u00e1n m\u00e1t v\u00e0 d\u00f9ng ngon nh\u1ea5t trong 2h sau khi nh\u1eadn h\u00e0ng.\n",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040272",
                "price": 0,
                "product_id": 2525,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2525
              }
            ]
          }
        ],
        "price": 19000,
        "base_price_str": "19.000\u0111",
        "price_str": "19.000\u0111",
        "base_price": 19000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1643102034_mochi-vietquat_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1643102034_mochi-vietquat.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "mochi-kem-viet-quat"
      },
      {
        "id": "6051cdb617a9cf0011ee6d8e",
        "recommended_id": null,
        "name": "Mochi Kem D\u1eeba D\u1ee9a",
        "description": "Bao b\u1ecdc b\u1edfi l\u1edbp v\u1ecf Mochi d\u1ebbo th\u01a1m, b\u00ean trong l\u00e0 l\u1edbp kem l\u1ea1nh c\u00f9ng nh\u00e2n d\u1eeba d\u1ee9a th\u01a1m l\u1eebng l\u1ea1 mi\u1ec7ng. G\u1ecdi 1 chi\u1ebfc Mochi cho ng\u00e0y th\u1eadt t\u01b0\u01a1i m\u00e1t. \nS\u1ea3n ph\u1ea9m ph\u1ea3i b\u1ea3o qu\u00e1n m\u00e1t v\u00e0 d\u00f9ng ngon nh\u1ea5t trong 2h sau khi nh\u1eadn h\u00e0ng.\n\n",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040274",
                "price": 0,
                "product_id": 2527,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2527
              }
            ]
          }
        ],
        "price": 19000,
        "base_price_str": "19.000\u0111",
        "price_str": "19.000\u0111",
        "base_price": 19000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1643101996_mochi-dua_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1643101996_mochi-dua.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "mochi-kem-dua-dua"
      },
      {
        "id": "5f60224ae31aed001153f816",
        "recommended_id": null,
        "name": "Mochi Kem Xo\u00e0i",
        "description": "Bao b\u1ecdc b\u1edfi l\u1edbp v\u1ecf Mochi d\u1ebbo th\u01a1m, b\u00ean trong l\u00e0 l\u1edbp kem l\u1ea1nh c\u00f9ng nh\u00e2n xo\u00e0i chua chua ng\u1ecdt ng\u1ecdt. G\u1ecdi 1 chi\u1ebfc Mochi cho ng\u00e0y th\u1eadt t\u01b0\u01a1i m\u00e1t. \nS\u1ea3n ph\u1ea9m ph\u1ea3i b\u1ea3o qu\u00e1n m\u00e1t v\u00e0 d\u00f9ng ngon nh\u1ea5t trong 2h sau khi nh\u1eadn h\u00e0ng.\n",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040244",
                "price": 0,
                "product_id": 2320,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2320
              }
            ]
          }
        ],
        "price": 19000,
        "base_price_str": "19.000\u0111",
        "price_str": "19.000\u0111",
        "base_price": 19000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1643101968_mochi-xoai_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1643101968_mochi-xoai.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "mochi-kem-xoai"
      },
      {
        "id": "5b03966a1acd4d5bbd6723ac",
        "recommended_id": null,
        "name": "Mousse G\u1ea5u Chocolate",
        "description": "V\u1edbi v\u1ebb ngo\u00e0i \u0111\u00e1ng y\u00eau v\u00e0 h\u01b0\u01a1ng v\u1ecb ng\u1ecdt ng\u00e0o, th\u01a1m b\u00e9o nh\u1ea5t \u0111\u1ecbnh b\u1ea1n ph\u1ea3i th\u1eed \u00edt nh\u1ea5t 1 l\u1ea7n.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "product_id": "467",
                "code": "11040010",
                "price": 0,
                "val": "V\u1eeba",
                "localize": {
                  "vi": "Size v\u1eeba",
                  "en": null
                },
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 467
              }
            ]
          }
        ],
        "price": 39000,
        "base_price_str": "39.000\u0111",
        "price_str": "39.000\u0111",
        "base_price": 39000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1638170067_gau_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1638170066_gau.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "mousse-gau-chocolate"
      },
      {
        "id": "5d92fbc79e12b47db8cabbd0",
        "recommended_id": null,
        "name": "Mousse Passion Cheese",
        "description": "T\u1eadn h\u01b0\u1edfng chi\u1ebfc b\u00e1nh m\u00e1t l\u1ea1nh v\u1edbi s\u1ef1 k\u1ebft h\u1ee3p ho\u00e0n h\u1ea3o c\u1ee7a v\u1ecb b\u00e9o ng\u1eady c\u1ee7a nh\u00e2n kem ph\u00f4 mai, c\u00e2n b\u1eb1ng v\u1edbi v\u1ecb chua thanh t\u1eeb chanh d\u00e2y.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040160",
                "price": 0,
                "product_id": 2100,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2100
              }
            ]
          }
        ],
        "price": 29000,
        "base_price_str": "29.000\u0111",
        "price_str": "29.000\u0111",
        "base_price": 29000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/5d92fbc79e12b47db8cabbd0_Chanh-day_994413_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/5d92fbc79e12b47db8cabbd0_Chanh-day_994413.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "mousse-passion-cheese"
      },
      {
        "id": "5d92fbc79e12b47db8cabbd1",
        "recommended_id": null,
        "name": "Mousse Tiramisu",
        "description": "H\u01b0\u01a1ng v\u1ecb d\u1ec5 ghi\u1ec1n \u0111\u01b0\u1ee3c t\u1ea1o n\u00ean b\u1edfi ch\u00fat \u0111\u1eafng nh\u1eb9 c\u1ee7a c\u00e0 ph\u00ea, l\u1edbp kem tr\u1ee9ng b\u00e9o ng\u1ecdt d\u1ecbu h\u1ea5p d\u1eabn",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040159",
                "price": 0,
                "product_id": 2099,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2099
              }
            ]
          }
        ],
        "price": 32000,
        "base_price_str": "32.000\u0111",
        "price_str": "32.000\u0111",
        "base_price": 32000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1638170137_tiramisu_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1638170137_tiramisu.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "mousse-tiramisu"
      },
      {
        "id": "5dd2087ff2546c2614fb08d1",
        "recommended_id": null,
        "name": "Mousse Red Velvet",
        "description": "B\u00e1nh nhi\u1ec1u l\u1edbp \u0111\u01b0\u1ee3c ph\u1ee7 l\u1edbp kem b\u00ean tr\u00ean b\u1eb1ng Cream cheese.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040151",
                "price": 0,
                "product_id": 1869,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 1869
              }
            ]
          }
        ],
        "price": 29000,
        "base_price_str": "29.000\u0111",
        "price_str": "29.000\u0111",
        "base_price": 29000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/5dd2087ff2546c2614fb08d1_Red-Velvet_087977_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/5dd2087ff2546c2614fb08d1_Red-Velvet_087977.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "mousse-red-velvet"
      },
      {
        "id": "5d2c4af1696fb3281b3f4ab9",
        "recommended_id": null,
        "name": "M\u00edt S\u1ea5y",
        "description": "M\u00edt s\u1ea5y kh\u00f4 v\u00e0ng \u01b0\u01a1m, gi\u00f2n r\u1ee5m, gi\u1eef nguy\u00ean \u0111\u01b0\u1ee3c v\u1ecb ng\u1ecdt l\u1ecbm c\u1ee7a m\u00edt t\u01b0\u01a1i.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11040193",
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "price": 0,
                "product_id": 11040193,
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 11040193
              }
            ]
          }
        ],
        "price": 20000,
        "base_price_str": "20.000\u0111",
        "price_str": "20.000\u0111",
        "base_price": 20000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/mit-say_666228_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/mit-say_666228.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "mit-say"
      }
    ]
  },
  {
    "style": 0,
    "id": 22,
    "name": "Combo",
    "title": "Combo",
    "thumbnail": "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/combo-moi.png",
    "slug": "combo-1",
    "products": [
      {
        "id": "61a4f0f461af54001a4619fc",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p C\u00e0 Ph\u00ea H\u00f2a Tan \u0110\u1eadm V\u1ecb Vi\u1ec7t",
        "description": "B\u1eaft \u0111\u1ea7u ng\u00e0y m\u1edbi v\u1edbi t\u00e1ch c\u00e0 ph\u00ea s\u1eefa \u201c\u0110\u1eadm v\u1ecb Vi\u1ec7t\u201d m\u1ea1nh m\u1ebd s\u1ebd gi\u00fap b\u1ea1n lu\u00f4n t\u1ec9nh t\u00e1o v\u00e0 h\u1ee9ng kh\u1edfi cho ng\u00e0y l\u00e0m vi\u1ec7c th\u1eadt hi\u1ec7u qu\u1ea3.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020147",
                "price": 0,
                "product_id": 2642,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2642
              }
            ]
          }
        ],
        "price": 109000,
        "base_price_str": "109.000\u0111",
        "price_str": "109.000\u0111",
        "base_price": 109000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641440252_1638341723-combo-ca-phe-3in1-damviviet_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641440252_1638341723-combo-ca-phe-3in1-damviviet.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-ca-phe-hoa-tan-dam-vi-viet"
      },
      {
        "id": "6196685bab06ba0019e2c39e",
        "recommended_id": null,
        "name": "Combo 2 C\u00e0 Ph\u00ea Rang Xay Original 1",
        "description": "C\u00e0 ph\u00ea Original 1 c\u1ee7a The Coffee House v\u1edbi th\u00e0nh ph\u1ea7n ch\u00ednh c\u00e0 ph\u00ea Robusta \u0110\u1eafk L\u1eafk, v\u00f9ng tr\u1ed3ng c\u00e0 ph\u00ea n\u1ed5i ti\u1ebfng nh\u1ea5t Vi\u1ec7t Nam. B\u1eb1ng c\u00e1ch \u00e1p d\u1ee5ng k\u1ef9 thu\u1eadt rang xay hi\u1ec7n \u0111\u1ea1i, C\u00e0 ph\u00ea Original 1 mang \u0111\u1ebfn tr\u1ea3i nghi\u1ec7m tuy\u1ec7t v\u1eddi khi u\u1ed1ng c\u00e0 ph\u00ea t\u1ea1i nh\u00e0 v\u1edbi h\u01b0\u01a1ng v\u1ecb \u0111\u1eadm \u0111\u00e0 truy\u1ec1n th\u1ed1ng h\u1ee3p kh\u1ea9u v\u1ecb c\u1ee7a gi\u1edbi tr\u1ebb s\u00e0nh c\u00e0 ph\u00ea.\n",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020142",
                "price": 0,
                "product_id": 2623,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2623
              }
            ]
          }
        ],
        "price": 99000,
        "base_price_str": "99.000\u0111",
        "price_str": "99.000\u0111",
        "base_price": 99000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1637288994_combo2-cforiginal-1_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1637288994_combo2-cforiginal-1.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-2-ca-phe-rang-xay-original-1"
      },
      {
        "id": "61224f81ef16be001293cccd",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p C\u00e0 Ph\u00ea S\u1eefa \u0110\u00e1 H\u00f2a Tan",
        "description": "Th\u1eadt d\u1ec5 d\u00e0ng \u0111\u1ec3 b\u1eaft \u0111\u1ea7u ng\u00e0y m\u1edbi v\u1edbi t\u00e1ch c\u00e0 ph\u00ea s\u1eefa \u0111\u00e1 s\u00f3ng s\u00e1nh, th\u01a1m ngon nh\u01b0 c\u00e0 ph\u00ea pha phin. V\u1ecb \u0111\u1eafng thanh c\u1ee7a c\u00e0 ph\u00ea ho\u00e0 quy\u1ec7n v\u1edbi v\u1ecb ng\u1ecdt b\u00e9o c\u1ee7a s\u1eefa, gi\u00fap b\u1ea1n lu\u00f4n t\u1ec9nh t\u00e1o v\u00e0 h\u1ee9ng kh\u1edfi cho ng\u00e0y l\u00e0m vi\u1ec7c th\u1eadt hi\u1ec7u qu\u1ea3.\n",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020119",
                "price": 0,
                "product_id": 2581,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2581
              }
            ]
          }
        ],
        "price": 109000,
        "base_price_str": "109.000\u0111",
        "price_str": "109.000\u0111",
        "base_price": 109000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/combo-3cfsd-nopromo_320619_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/combo-3cfsd-nopromo_320619.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-ca-phe-sua-da-hoa-tan"
      },
      {
        "id": "61a4f0f361af54001a4619f8",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p Tr\u00e0 Oolong T\u00fai L\u1ecdc Tearoma",
        "description": "Tr\u00e0 Oolong t\u00fai l\u1ecdc v\u1edbi m\u00f9i h\u01b0\u01a1ng d\u1ecbu nh\u1eb9 ho\u00e0n to\u00e0n t\u1eeb t\u1ef1 nhi\u00ean, v\u1ecb h\u1eadu ng\u1ecdt, v\u00e0 h\u01b0\u01a1ng th\u01a1m tinh t\u1ebf. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0,... nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020151",
                "price": 0,
                "product_id": 2646,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2646
              }
            ]
          }
        ],
        "price": 69000,
        "base_price_str": "69.000\u0111",
        "price_str": "69.000\u0111",
        "base_price": 69000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641440292_1638341615-combo-tra-oolong-400x400_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641440292_1638341615-combo-tra-oolong-400x400.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-tra-oolong-tui-loc-tearoma"
      },
      {
        "id": "61a4f0f461af54001a4619f9",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p Tr\u00e0 L\u00e0i T\u00fai L\u1ecdc Tearoma",
        "description": "Tr\u00e0 t\u00fai l\u1ecdc Tearoma h\u01b0\u01a1ng l\u00e0i th\u01a1m tinh t\u1ebf, thanh m\u00e1t, tr\u00ean n\u1ec1n tr\u00e0 xanh \u0111\u1eadm \u0111\u00e0 kh\u00f3 qu\u00ean. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0,.. nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020150",
                "price": 0,
                "product_id": 2645,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2645
              }
            ]
          }
        ],
        "price": 69000,
        "base_price_str": "69.000\u0111",
        "price_str": "69.000\u0111",
        "base_price": 69000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641440398_1638341675-combo-tra-huong-lai_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641440398_1638341675-combo-tra-huong-lai.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-tra-lai-tui-loc-tearoma"
      },
      {
        "id": "61a4f0f461af54001a4619fa",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p Tr\u00e0 Sen T\u00fai L\u1ecdc Tearoma",
        "description": "Tr\u00e0 t\u00fai l\u1ecdc Tearoma h\u01b0\u01a1ng sen tinh t\u1ebf, thanh m\u00e1t, tr\u00ean n\u1ec1n tr\u00e0 xanh \u0111\u1eadm \u0111\u00e0 kh\u00f3 qu\u00ean. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0, \u0111i du l\u1ecbch,... nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020149",
                "price": 0,
                "product_id": 2644,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2644
              }
            ]
          }
        ],
        "price": 69000,
        "base_price_str": "69.000\u0111",
        "price_str": "69.000\u0111",
        "base_price": 69000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641440433_1638341689-combo-tra-huong-sen-400x400_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641440433_1638341689-combo-tra-huong-sen-400x400.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-tra-sen-tui-loc-tearoma"
      },
      {
        "id": "61a4f0f461af54001a4619fb",
        "recommended_id": null,
        "name": "Combo 3 H\u1ed9p Tr\u00e0 \u0110\u00e0o T\u00fai L\u1ecdc Tearoma",
        "description": "Tr\u00e0 t\u00fai l\u1ecdc Tearoma h\u01b0\u01a1ng \u0111\u00e0o v\u1edbi h\u01b0\u01a1ng th\u01a1m tinh t\u1ebf v\u00e0 ho\u00e0n to\u00e0n t\u1ef1 nhi\u00ean, c\u00f9ng n\u1ec1n tr\u00e0 \u0111en \u0111\u1eadm v\u1ecb kh\u00f3 qu\u00ean. Tr\u00e0 t\u00fai l\u1ecdc Tearoma ti\u1ec7n l\u1ee3i \u0111\u1ec3 s\u1eed d\u1ee5ng t\u1ea1i v\u0103n ph\u00f2ng, t\u1ea1i nh\u00e0,.. nh\u01b0ng v\u1eabn \u0111\u1ea3m b\u1ea3o \u0111\u01b0\u1ee3c ch\u1ea5t l\u01b0\u1ee3ng v\u1ec1 h\u01b0\u01a1ng tr\u00e0 tinh t\u1ebf, v\u1ecb tr\u00e0 \u0111\u1eadm \u0111\u00e0.",
        "options": [
          {
            "group_id": 0,
            "name": "Size",
            "description": "Ch\u1ecdn 1 lo\u1ea1i size",
            "min": 1,
            "max": 1,
            "default_index": 0,
            "items": [
              {
                "code": "11020148",
                "price": 0,
                "product_id": 2643,
                "localize": {
                  "en": "V\u1eeba",
                  "vi": "V\u1eeba"
                },
                "val": "V\u1eeba",
                "is_main": true,
                "price_str": "+ 0\u0111",
                "name": "V\u1eeba",
                "id": 2643
              }
            ]
          }
        ],
        "price": 69000,
        "base_price_str": "69.000\u0111",
        "price_str": "69.000\u0111",
        "base_price": 69000,
        "thumbnail": "https://minio.thecoffeehouse.com/image/admin/1641440460_1638344058-combo-tra-huong-dao-400x400_400x400.jpg",
        "images": [
          "https://minio.thecoffeehouse.com/image/admin/1641440460_1638344058-combo-tra-huong-dao-400x400.jpg"
        ],
        "is_pickup": true,
        "is_delivery": true,
        "hint_note": [],
        "promotion": null,
        "status": 1,
        "description_html": null,
        "slug": "combo-3-hop-tra-dao-tui-loc-tearoma"
      }
    ]
  }
]
